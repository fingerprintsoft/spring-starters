package co.fingerprintsoft.notification.bulksms;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.MultipartBody;
import con.fingerprintsoft.notification.sms.SMS;
import con.fingerprintsoft.notification.sms.SMSResponse;
import con.fingerprintsoft.notification.sms.SMSSender;
import con.fingerprintsoft.notification.sms.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BulkSMSSender implements SMSSender {
    private static final Logger LOG = LoggerFactory.getLogger(BulkSMSSender.class);

    private BulkSMSSettings settings;

    public BulkSMSSender(BulkSMSSettings settings) {
        this.settings = settings;
    }

    @Override
    public SMSResponse sendSingleSMS(SMS sms) {

        MultipartBody requestWithBody = Unirest.post(settings.getEapiUrl())
                .field("username", settings.getUsername())
                .field("password", settings.getPassword())
                .field("message", sms.getBody())
                .field("want_report", "1")
                .field("msisdn", sms.getTo())
                .field("source_id", sms.getId())
                .field("stop_dup_id", sms.getId())
                .field("allow_concat_text_sms", "1")
                .field("concat_text_sms_max_parts", "3");

        if (settings.getTestAlwaysSucceed() != null) {
            requestWithBody.field("test_always_succeed", settings.getTestAlwaysSucceed());
        }
        if (settings.getTestAlwaysFail() != null) {
            requestWithBody.field("test_always_fail", settings.getTestAlwaysFail());
        }

        return getSMSResponse(requestWithBody);
    }

    private SMSResponse getSMSResponse(MultipartBody requestWithBody) {

        HttpResponse<String> request = null;
        try {
            request = requestWithBody.asString();
        } catch (UnirestException e) {
            throw new RuntimeException(e);
        }

        BulkSMSSMSResponse smsResponse;
        switch (request.getStatus()) {
            case 200:
                String responseDetails = request.getBody();
                String[] response = responseDetails.split("\\|");
                int statusCode = Integer.valueOf(response[0]);
                String statusDescription = response[1];
                String batchId = response[2];

                smsResponse = new BulkSMSSMSResponse();
                switch (statusCode) {
                    case 0:
                    case 1:
                        smsResponse.setStatus(Status.SUCCESSFULL);
                        break;
                    default:
                        smsResponse.setStatus(Status.FAILED);
                        smsResponse.setErrorCode(statusCode);
                }
                smsResponse.setHttpStatus(request.getStatus());
                smsResponse.setMessage(statusDescription);
                smsResponse.setExternalReference(batchId);
                return smsResponse;
            default:
                smsResponse = new BulkSMSSMSResponse();
                smsResponse.setHttpStatus(request.getStatus());
                smsResponse.setStatus(Status.FAILED);
                return smsResponse;
        }
    }

}