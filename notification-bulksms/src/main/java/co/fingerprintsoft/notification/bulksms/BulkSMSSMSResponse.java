package co.fingerprintsoft.notification.bulksms;

import con.fingerprintsoft.notification.sms.SMSResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BulkSMSSMSResponse extends SMSResponse {
}
