package co.fingerprintsoft.notification.bulksms;

import con.fingerprintsoft.notification.sms.SMS;
import lombok.*;

@Data
@NoArgsConstructor
@ToString
public class BulkSMSSingle {

    @NonNull
    private String to;
    @NonNull
    private String body;
    private String from;
    private String deliveryReports;
    private String userSuppliedId;

    public BulkSMSSingle(String to, String body) {
        this.to = to;
        this.body = body;
    }

    public static BulkSMSSingle create(SMS sms) {
        BulkSMSSingle single = new BulkSMSSingle(sms.getTo().get(0), sms.getBody());
        single.setFrom(sms.getFrom());
        single.setUserSuppliedId(sms.getId());
        if(sms.isReports()) {
            single.setDeliveryReports("ALL");
        }
        return single;
    }

}
