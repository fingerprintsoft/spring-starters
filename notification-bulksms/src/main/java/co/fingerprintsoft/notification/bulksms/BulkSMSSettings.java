package co.fingerprintsoft.notification.bulksms;


import lombok.Data;

@Data
public class BulkSMSSettings {

    private String eapiUrl = "http://bulksms.2way.co.za/eapi/submission/send_sms/2/2.0";
    private String username;
    private String password;
    private Integer testAlwaysFail;
    private Integer testAlwaysSucceed;
}
