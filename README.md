![Spring Boot Starters](SpringStarters-06.svg){width=251 height=156}

# notification-spring-boot-starter
**[Notification Support](https://bitbucket.org/fingerprintsoft/spring-starters/overview)** Integration for Spring Boot. 

Notifications Spring Boot provides Clickatell, BulkSMS, Mailgun and Apple Push support for notifications in Spring Boot Applications.
To integrate notification-spring-boot-starter in your project:
Simply add the starter jar notification-spring-boot-starter to your classpath if using @SpringBootApplication or @EnableAutoConfiguration will enable notifications across the entire Spring Environment
    
## Support
| Version       | Boot Version  |
| ------------- |:-------------:|
| 1.0.x         | 1.3.x         |
| 1.1.x         | 1.4.x         |
| 1.2.x         | 1.5.x         |
| 2.0.x         | 2.0.8+        |
| 2.1.x         | 2.1.13+       |
| 2.2.x         | 2.2.5+        |
| 2.3.x         | 2.3.11+       |
| 2.4.x         | 2.4.6+        |
| 2.5.x         | 2.5.0+        |

## Test Errors
Current test failures, need to review.

    Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.003 sec

    Results :

    Failed tests:   testErrorResponseMissingEmail(co.fingerprintsoft.payment.paygate.api.PayGatePaymentServiceTest): expected:<FAILURE> but was:<SUCCESS>
    testErrorResponseInvalidPayGateId(co.fingerprintsoft.payment.paygate.api.PayGatePaymentServiceTest): expected:<[ND_INV_PGID]> but was:<[DATA_CHK]>

    Tests run: 55, Failures: 2, Errors: 0, Skipped: 0

## Quick Start
Update 2/10/2017: notification-spring-boot-starter is now in Maven Central!
    
Simply add the starter jar dependency and a notification method to your project if your Spring Boot application uses @SpringBootApplication or @EnableAutoConfiguration and notifications will be enabled across the entire Spring Environment:
    
    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-spring-boot-starter</artifactId>
      <version>${version}</version>
    </dependency>

    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-mailgun</artifactId>
      <version>${version}</version>
    </dependency>

### Mailgun
Add the mailgun dependency

    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-mailgun</artifactId>
      <version>${version}</version>
    </dependency>

### BulkSMS
Add the bulksms dependency

    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-bulksms</artifactId>
      <version>${version}</version>
    </dependency>
    
### Clickatell
Add the clickatell dependency

    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-clickatell</artifactId>
      <version>${version}</version>
    </dependency>
    
    
### Apple Push
Add the apns dependency

    <dependency>
      <groupId>co.fingerprintsoft</groupId>
      <artifactId>notification-apns</artifactId>
      <version>${version}</version>
    </dependency>
    
    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-tcnative-boringssl-static</artifactId>
      <version>${netty-tcnative.version}</version>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-tcnative</artifactId>
      <classifier>linux-x86_64</classifier>
      <version>${netty-tcnative.version}</version>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-codec-http</artifactId>
      <version>${netty.version}</version>
    </dependency>
    <dependency>
      <groupId>io.netty</groupId>
      <artifactId>netty-handler-proxy</artifactId>
      <version>${netty.version}</version>
    </dependency>
    
### Getting a handle on the senders

    @Autowired
    private EmailSender emailSender;
    @Autowired
    private SMSSender smsSender;
    @Autowired
    private PushNotificationSender pushNotificationSender;

### Sending an SMS

    SMS sms = new SMS();
    sms.setBody("Test");
    sms.setFrom("+27821111111");
    sms.setTo(Arrays.asList(new String []{"+2782111111"}));
    sms.setId("1");
    SMSResponse smsResponse = smsSender.sendSingleSMS(sms);

### Sending an Email

    Email email = new Email();
    email.setSubject("This is a Test");
    email.setMessage("Testing Spring boot starter.");
    EmailResponse emailResponse = emailSender.sendPlainText(email);
        
### Sending an Apple Push Notification

    NotificationResult result = pushNotificationSender
        .sendNotification(
          "This is a test", "<insert device token>"
    );


