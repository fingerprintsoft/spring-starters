package co.fingerprintsoft.notification.clickatell;

import con.fingerprintsoft.notification.sms.SMSResponse;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ClickatellSMSResponse extends SMSResponse {
}
