package co.fingerprintsoft.notification.clickatell;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * A Java representation of a JSON Response for Clickatel RESTful services, pertaining to the dat section of the response.
 * <p>
 * "message":[
 * {
 * "accepted":true,
 * "to":"27999000001",
 * "apiMessageId":"0a53832f15bb444d8f73a66e3c4ff7a0"
 * }
 * ]
 */
@Data
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Value {

    /**
     * The list of messages that were sent to recipients
     */
    private List<Message> message;

}
