package co.fingerprintsoft.notification.clickatell;

import lombok.Data;
import lombok.ToString;

/**
 * A response from sending an SMS to a recipient.
 */
@Data
@ToString
public class Message {

    /**
     * The success status of the message
     */
    private Boolean accepted;
    /**
     * The recipient
     */
    private String to;

    private String apiMessageId;
    private String error;

}
