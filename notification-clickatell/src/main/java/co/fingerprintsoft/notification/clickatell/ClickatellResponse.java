package co.fingerprintsoft.notification.clickatell;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

/**
 * A Java representation of a JSON Response for Clickatel RESTful services
 * <p>
 * {
 * "data":{
 * "message":[
 * {
 * "accepted":true,
 * "to":"27999000001",
 * "apiMessageId":"0a53832f15bb444d8f73a66e3c4ff7a0"
 * }
 * ]
 * }
 * }
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class ClickatellResponse {

    /**
     * The data attribute of  the response
     */
    private Value data;

}
