package co.fingerprintsoft.notification.clickatell;

import lombok.Data;

@Data
public class ClickatellSettings {

    private String apiId;
    private String token;
    private String url;
    private String version;
    private String grantType;

}
