package co.fingerprintsoft.notification.clickatell;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * A Java representation of a JSON Payload for Clickatell RESTful services
 * <p>
 * {"text":"Test Message","to":["27999000001"]}
 */
@Data
@ToString
public class ClickatellSMS {

    /**
     * The test to send
     */
    private String text;
    /**
     * The list of recipients
     */
    private List<String> to = new ArrayList();

    /**
     * Add a recipient
     *
     * @param to the recipient to add
     */
    public void addTo(String to) {
        this.to.add(to);
    }


}
