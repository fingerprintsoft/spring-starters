package co.fingerprintsoft.notification.mailgun;

import lombok.Data;

@Data
public class MailGunSettings {

    private String apiKey;
    private String url;
    private String version = "v3";
    private String domain;
    private String from;

    //this is used for testing so no emails go out by mistake
    private String testModeTo;
    private boolean testMode;




}
