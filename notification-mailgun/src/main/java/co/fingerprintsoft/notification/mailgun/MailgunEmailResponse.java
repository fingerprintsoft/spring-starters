package co.fingerprintsoft.notification.mailgun;

import co.fingerprintsoft.notification.email.EmailResponse;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class MailgunEmailResponse extends EmailResponse {
}
