package co.fingerprintsoft.notification.mailgun;

import co.fingerprintsoft.notification.email.Email;
import co.fingerprintsoft.notification.email.EmailResponse;
import co.fingerprintsoft.notification.email.EmailSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class MailGunSender implements EmailSender {

    private static final Logger LOG = LoggerFactory.getLogger(MailGunSender.class);

    private MailGunSettings mailGunSettings;
    private com.fasterxml.jackson.databind.ObjectMapper objectMapper;

    public MailGunSender(MailGunSettings mailGunSettings, com.fasterxml.jackson.databind.ObjectMapper objectMapper) {
        this.mailGunSettings = mailGunSettings;
        this.objectMapper = objectMapper;
        Unirest.setObjectMapper(new ObjectMapper() {

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return objectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return objectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public EmailResponse sendPlainText(Email email) {

        HttpRequestWithBody requestWithBody = Unirest.post("https://{url}/{version}/{domain}/messages")
                .routeParam("domain", mailGunSettings.getDomain())
                .routeParam("url", mailGunSettings.getUrl())
                .routeParam("version", mailGunSettings.getVersion())
                .basicAuth("api", mailGunSettings.getApiKey())
                .queryString("subject", email.getSubject())
                .queryString("text", email.getMessage());

        if (email.getFrom() != null) {
            requestWithBody.queryString("from", email.getFrom());
        } else {
            requestWithBody.queryString("from", mailGunSettings.getFrom());
        }

        if (mailGunSettings.isTestMode()) {
            requestWithBody.queryString("to", mailGunSettings.getTestModeTo());
        } else {
            requestWithBody.queryString("to", email.getTo());
        }

        if(mailGunSettings.isTestMode()) {
            requestWithBody.field("o:testmode", "true");
        }


        return getEmailResponse(requestWithBody);

    }

    public EmailResponse sendComplexMessage(Email email) {

        HttpRequestWithBody requestWithBody = Unirest.post("https://{url}/{version}/{domain}/messages")
                .routeParam("domain", mailGunSettings.getDomain())
                .routeParam("url", mailGunSettings.getUrl())
                .routeParam("version", mailGunSettings.getVersion())
                .basicAuth("api", mailGunSettings.getApiKey());

        if (email.getFrom() != null) {
            requestWithBody.queryString("from", email.getFrom());
        } else {
            requestWithBody.queryString("from", mailGunSettings.getFrom());
        }

        if (mailGunSettings.isTestMode()) {
            requestWithBody.queryString("to", mailGunSettings.getTestModeTo());
        } else {
            requestWithBody.queryString("to", email.getTo());
        }

        if (email.getCc() != null) {
            requestWithBody.queryString("cc", email.getCc());
        }
        if (email.getBcc() != null) {
            requestWithBody.queryString("bcc", email.getBcc());
        }
        if (email.getSubject() != null) {
            requestWithBody.queryString("subject", email.getSubject());
        }
        if (email.getMessage() != null) {
            requestWithBody.queryString("text", email.getMessage());
        }
        if (email.getHtml() != null) {
            requestWithBody.queryString("html", email.getHtml());
        }

        for (File attachment : email.getAttachments()) {
            requestWithBody.field("attachment", attachment);
        }

        if(mailGunSettings.isTestMode()) {
            requestWithBody.field("o:testmode", "true");
        }

        return getEmailResponse(requestWithBody);
    }

    private EmailResponse getEmailResponse(HttpRequestWithBody requestWithBody) {
        HttpResponse<String> request = null;
        try {
            request = requestWithBody.asString();
        } catch (UnirestException e) {
            throw new RuntimeException(e);
        }

        switch (request.getStatus()) {
            case 200:
            case 400:
                String response = request.getBody();
                try {
                    MailgunEmailResponse emailResponse = objectMapper.readValue(response, MailgunEmailResponse.class);
                    emailResponse.setStatus(request.getStatus());
                    return emailResponse;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            default:
                MailgunEmailResponse emailResponse = new MailgunEmailResponse();
                emailResponse.setStatus(request.getStatus());
                return emailResponse;
        }
    }
}
