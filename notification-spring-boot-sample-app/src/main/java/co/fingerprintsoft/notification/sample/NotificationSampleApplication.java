package co.fingerprintsoft.notification.sample;

import co.fingerprintsoft.notification.NotificationResult;
import co.fingerprintsoft.notification.PushNotificationSender;
import co.fingerprintsoft.notification.email.Email;
import co.fingerprintsoft.notification.email.EmailResponse;
import co.fingerprintsoft.notification.email.EmailSender;

import con.fingerprintsoft.notification.sms.SMS;
import con.fingerprintsoft.notification.sms.SMSResponse;
import con.fingerprintsoft.notification.sms.SMSSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class NotificationSampleApplication implements CommandLineRunner {

    @Autowired
    private EmailSender emailSender;
    @Autowired
    private SMSSender smsSender;
    @Autowired
    private PushNotificationSender pushNotificationSender;


    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(NotificationSampleApplication.class, args);
        context.close();
    }

    @Override
    public void run(String... args) throws Exception {
        Email email = new Email();
        email.setSubject("This is a Test");
        email.setMessage("Testing Spring boot starter.");
        EmailResponse emailResponse = emailSender.sendPlainText(email);
        System.out.println(emailResponse);
        SMS sms = new SMS();
        sms.setBody("Test");
        sms.setFrom("+27829366720");
        sms.setTo(Arrays.asList(new String []{"+27829266720"}));
        sms.setId("1");
        SMSResponse smsResponse = smsSender.sendSingleSMS(sms);
        System.out.println(smsResponse);

        NotificationResult result = pushNotificationSender
                .sendNotification("This is a test", "<insert device token>");
        System.out.println(result);

    }
}