package co.fingerprintsoft.payment.paygate;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "payment.paygate")
@EqualsAndHashCode(of={"returnUrl","notifyUrl","payGateId","encryptionKey"})
public class PayGateProperties {

    private String returnUrl;
    private String notifyUrl;
    private Long payGateId;
    private String encryptionKey;
}
