package co.fingerprintsoft.payment.paygate;

import co.fingerprintsoft.payment.paygate.api.PayGateSettings;
import co.fingerprintsoft.payment.paygate.api.PayGatePaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(PayGatePaymentService.class)
@EnableConfigurationProperties(PayGateProperties.class)
public class PayGateAutoConfiguration {

    @Autowired
    private PayGateProperties payGateProperties;

    public PayGateAutoConfiguration() {
    }

    public PayGateAutoConfiguration(PayGateProperties notificationProperties) {
        this.payGateProperties = notificationProperties;
    }

    @Bean
    @ConditionalOnClass(PayGateSettings.class)
    public PayGateSettings payGateSettings() {

        String encryptionKey = payGateProperties.getEncryptionKey();
        String notifyUrl = payGateProperties.getNotifyUrl();
        String returnUrl = payGateProperties.getReturnUrl();
        Long payGateId = payGateProperties.getPayGateId();

        PayGateSettings payGateSettings = new PayGateSettings();
        payGateSettings.setEncryptionKey(encryptionKey);
        payGateSettings.setNotifyUrl(notifyUrl);
        payGateSettings.setReturnUrl(returnUrl);
        payGateSettings.setPayGateId(payGateId);

        return payGateSettings;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(PayGatePaymentService.class)
    @ConditionalOnBean({PayGateSettings.class})
    public PayGatePaymentService paymentService(PayGateSettings payGateSettings) {
        return new PayGatePaymentService(payGateSettings);
    }

}
