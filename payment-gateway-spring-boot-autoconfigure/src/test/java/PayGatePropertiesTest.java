import org.junit.Assert;
import org.junit.Test;

import co.fingerprintsoft.payment.paygate.PayGateProperties;

public class PayGatePropertiesTest {

    @Test
    public void testConstructor () {
        PayGateProperties payGateProperties = new PayGateProperties();
        Assert.assertNull(payGateProperties.getNotifyUrl());
        Assert.assertNull(payGateProperties.getPayGateId());
        Assert.assertNull(payGateProperties.getEncryptionKey());
        Assert.assertNull(payGateProperties.getReturnUrl());
    }

    @Test
    public void testEqualTrue(){
        PayGateProperties actual = new PayGateProperties();
        PayGateProperties expected = new PayGateProperties();

        actual.setPayGateId(2L);
        actual.setEncryptionKey("secret");
        actual.setNotifyUrl("https//notify_url/test");
        actual.setReturnUrl("https//return_url/test");

        expected.setPayGateId(2L);
        expected.setEncryptionKey("secret");
        expected.setNotifyUrl("https//notify_url/test");
        expected.setReturnUrl("https//return_url/test");

        Assert.assertEquals(actual,expected);
    }

    @Test
    public void testNotEqualPayGateIdNotSame(){
        PayGateProperties payGatePropertiesOne = new PayGateProperties();
        PayGateProperties payGatePropertiesTwo = new PayGateProperties();

        payGatePropertiesOne.setPayGateId(1L);
        payGatePropertiesOne.setEncryptionKey("secret");
        payGatePropertiesOne.setNotifyUrl("https//notify_url/test");
        payGatePropertiesOne.setReturnUrl("https//return_url/test");

        payGatePropertiesTwo.setPayGateId(2L);
        payGatePropertiesTwo.setEncryptionKey("secret");
        payGatePropertiesTwo.setNotifyUrl("https//notify_url/test");
        payGatePropertiesTwo.setReturnUrl("https//return_url/test");

        Assert.assertNotEquals(payGatePropertiesOne.getPayGateId(),payGatePropertiesTwo.getPayGateId());
        Assert.assertNotEquals(payGatePropertiesOne,payGatePropertiesTwo);
    }

    @Test
    public void testNotEqualsNotifyUrlNotSame (){
        PayGateProperties payGatePropertiesOne = new PayGateProperties();
        PayGateProperties payGatePropertiesTwo = new PayGateProperties();

        payGatePropertiesOne.setPayGateId(2L);
        payGatePropertiesOne.setEncryptionKey("secret");
        payGatePropertiesOne.setNotifyUrl("https//notify_url/test");
        payGatePropertiesOne.setReturnUrl("https//return_url/test");

        payGatePropertiesTwo.setPayGateId(2L);
        payGatePropertiesTwo.setEncryptionKey("secret");
        payGatePropertiesTwo.setNotifyUrl("https//notify_url/test/thank-you");
        payGatePropertiesTwo.setReturnUrl("https//return_url/test");

        Assert.assertNotEquals(payGatePropertiesOne.getNotifyUrl(),payGatePropertiesTwo.getNotifyUrl());
        Assert.assertNotEquals(payGatePropertiesOne,payGatePropertiesTwo);
    }

    @Test
    public void testNotEqualsReturnUrlNotSame (){
        PayGateProperties payGatePropertiesOne = new PayGateProperties();
        PayGateProperties payGatePropertiesTwo = new PayGateProperties();

        payGatePropertiesOne.setPayGateId(2L);
        payGatePropertiesOne.setEncryptionKey("secret");
        payGatePropertiesOne.setNotifyUrl("https//notify_url/test");
        payGatePropertiesOne.setReturnUrl("https//return_url/test");

        payGatePropertiesTwo.setPayGateId(2L);
        payGatePropertiesTwo.setEncryptionKey("secret");
        payGatePropertiesTwo.setNotifyUrl("https//notify_url/test");
        payGatePropertiesTwo.setReturnUrl("https//return_url/test/thank-you");

        Assert.assertNotEquals(payGatePropertiesOne.getReturnUrl(),payGatePropertiesTwo.getReturnUrl());
        Assert.assertNotEquals(payGatePropertiesOne,payGatePropertiesTwo);
    }

    @Test
    public void testNotEqualsEncryptionKey (){
        PayGateProperties payGatePropertiesOne = new PayGateProperties();
        PayGateProperties payGatePropertiesTwo = new PayGateProperties();


        payGatePropertiesOne.setPayGateId(2L);
        payGatePropertiesOne.setEncryptionKey("secret");
        payGatePropertiesOne.setNotifyUrl("https//notify_url/test");
        payGatePropertiesOne.setReturnUrl("https//return_url/test");

        payGatePropertiesTwo.setPayGateId(2L);
        payGatePropertiesTwo.setEncryptionKey("secretTwo");
        payGatePropertiesTwo.setNotifyUrl("https//notify_url/test");
        payGatePropertiesTwo.setReturnUrl("https//return_url/test/thank-you");

        Assert.assertNotEquals(payGatePropertiesOne.getEncryptionKey(),payGatePropertiesTwo.getEncryptionKey());
        Assert.assertNotEquals(payGatePropertiesOne,payGatePropertiesTwo);
    }
}
