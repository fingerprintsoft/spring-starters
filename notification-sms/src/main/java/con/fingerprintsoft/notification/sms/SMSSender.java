package con.fingerprintsoft.notification.sms;

public interface SMSSender {
    SMSResponse sendSingleSMS(SMS sms);
}
