package con.fingerprintsoft.notification.sms;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class SMSResponse {

    private String id;
    private String message;
    private Status status;
    private Integer httpStatus;
    private Integer errorCode;
    private String externalReference;

}

