package con.fingerprintsoft.notification.sms;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class SMS {
    private String id;
    private String from;
    private List<String> to;
    private String body;
    private boolean reports;
}