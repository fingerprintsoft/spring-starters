## Deployments
1. Build Latest Project 
   >``git checkout develop``
   >``mvn jgitflow:release-start``
2. Wait for build to complete 
   >``mvn jgitflow:release-finish``

## OSSRH Stuff comes here     
mvn clean build
mvn verify deploy -Dskip-notification-deploy=false -Dskip-payment-gateway-deploy=false -Prelease
