package co.fingerprintsoft.payment;

import co.fingerprintsoft.payment.paygate.api.PayGatePaymentService;
import co.fingerprintsoft.payment.paygate.api.PayGateSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PaymentGatewaySampleApplication implements CommandLineRunner {

    @Autowired
    private PayGateSettings payGateSettings;
    @Autowired
    private PayGatePaymentService payGatePaymentService;


    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(PaymentGatewaySampleApplication.class, args);
        context.close();
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println(payGatePaymentService);
        System.out.println(payGateSettings);

    }
}