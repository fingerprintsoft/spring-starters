package co.fingerprintsoft.payment.paygate.domain;

import org.junit.Assert;
import org.junit.Test;

public class StatusCheckRequestTest {
    @Test
    public void testConstructor() {
        TransactionStatusRequest StatusCheck = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8", "PayGate Test");

        Assert.assertNotNull(StatusCheck.getPayRequestId());
    }

    @Test
    public void testEqualsTrue() {
        TransactionStatusRequest actual = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                                                           "PayGate Test"
        );
        TransactionStatusRequest expected = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                                                             "PayGate Test"
        );

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testNotEqualsPayRequestIdNotSame() {
        TransactionStatusRequest statusCheckOne = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                                   "PayGate Test"
        );
        TransactionStatusRequest statusCheckTwo = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                                                                   "PayGate Test"
        );

        Assert.assertNotEquals(statusCheckOne.getPayRequestId(), statusCheckTwo.getPayRequestId());
        Assert.assertNotEquals(statusCheckOne, statusCheckTwo);


    }

    @Test
    public void testCalculateChecksum() throws Exception {
        String key = "secret";
        Long payGateId = 10011072130L;
        TransactionStatusRequest statusCheckOne = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                                   "PayGate Test"
        );

        Assert.assertEquals("f5563213b72cb405167ba53e8c3ee466", statusCheckOne.calculateChecksum(key, payGateId));
    }

}
