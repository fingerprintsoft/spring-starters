package co.fingerprintsoft.payment.paygate.domain;


import org.junit.Assert;
import org.junit.Test;

public class PaymentResponseTest {

    @Test
    public void testConstructor() {
        PaymentResponse responce = new PaymentResponse(2L, "Abc1234", "fbc1234bd", "800");

        Assert.assertNotNull(responce.getPayGateId());
        Assert.assertNotNull(responce.getPayRequestId());
        Assert.assertNotNull(responce.getReference());
        Assert.assertNotNull(responce.getChecksum());
    }

    @Test
    public void testEqualsTrue() {

        PaymentResponse expected = new PaymentResponse(2L, "Abc1234", "fbc1234bd", "800");
        PaymentResponse actual = new PaymentResponse(2L, "Abc1234", "fbc1234bd", "900");

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testNotEqualPayGateIdNotSame() {

        PaymentResponse responseOne = new PaymentResponse(2L, "Abc1234", "fbc1234bd", "800");
        PaymentResponse responseTwo = new PaymentResponse(4L, "Abc1234", "fbc1234bd", "900");

        Assert.assertNotEquals(responseOne.getPayGateId(), responseTwo.getPayGateId());
        Assert.assertNotEquals(responseOne, responseTwo);
    }

    @Test
    public void testNotEqualReferenceNotSame() {
        PaymentResponse responseOne = new PaymentResponse(4L, "Abc1234e", "fbc1234bd", "800");
        PaymentResponse responseTwo = new PaymentResponse(4L, "Abc1234", "fbc1234bdg", "900");

        Assert.assertNotEquals(responseOne.getReference(), responseTwo.getReference());
        Assert.assertNotEquals(responseOne, responseTwo);
    }

    @Test
    public void testPaymentRequestIdNotSame() {
        PaymentResponse responseOne = new PaymentResponse(5L, "Abc12349er", "fbc1234bd", "800");
        PaymentResponse responseTwo = new PaymentResponse(5L, "Abc1234", "fbc1234bd", "900");

        Assert.assertNotEquals(responseOne.getPayRequestId(), responseTwo.getPayRequestId());
        Assert.assertNotEquals(responseOne, responseTwo);

    }

    @Test
    public void testCalculateChecksum() throws Exception {

        String key = "secret";
        PaymentResponse responseOne = new PaymentResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                          "PayGate Test", "f5563213b72cb405167ba53e8c3ee466"
        );

        Assert.assertEquals(responseOne.getChecksum(), responseOne.calculateChecksum(key));
    }

    @Test
    public void testVerifyCheckSumTrue() throws Exception {
        String key = "secret";
        PaymentResponse responseOne = new PaymentResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                          "PayGate Test", "f5563213b72cb405167ba53e8c3ee466"
        );
        Assert.assertTrue(responseOne.verifyChecksum(key));
    }

    @Test
    public void testVerifyCheckSuFalse() throws Exception {
        String key = "secret";
        PaymentResponse responseOne = new PaymentResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                          "PayGate Test", "f5563213b72cb405167ba53e8c3ee467"
        );
        Assert.assertFalse(responseOne.verifyChecksum(key));
    }


    @Test
    public void testUpdateCheckSumMethod() throws Exception {
        String key="secret";
        PaymentResponse responseOne = new PaymentResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                                                          "PayGate Test", ""
        );
        responseOne.updateChecksum(key);
        Assert.assertNotEquals("",responseOne.getChecksum());
    }


}
