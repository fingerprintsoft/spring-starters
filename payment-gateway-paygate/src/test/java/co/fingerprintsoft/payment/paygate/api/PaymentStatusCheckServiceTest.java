package co.fingerprintsoft.payment.paygate.api;

import co.fingerprintsoft.payment.paygate.Country;
import co.fingerprintsoft.payment.paygate.Currency;
import co.fingerprintsoft.payment.paygate.Locale;
import co.fingerprintsoft.payment.paygate.domain.Error;
import co.fingerprintsoft.payment.paygate.domain.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

public class PaymentStatusCheckServiceTest {

    private PayGatePaymentService paymentRequestService;
    private PayGateSettings settings;

    @Before
    public void setup() {
        settings = new PayGateSettings(10011072130L, "secret", "https://www.paygate.co.za/thankyou", null);
        paymentRequestService = new PayGatePaymentService(settings);
    }


    @Test
    public void testStatusCheckReturnedInvalidPayRequestId() throws Exception {
        PayGatePaymentService paymentRequestService = new PayGatePaymentService(settings);

        TransactionStatusRequest paymentStatus = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                "PayGateTest"
        );
        NotifyResponse actual = paymentRequestService.paymentStatusCheck(paymentStatus);


        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getHttpStatus());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertNotNull(actual.getStatus());
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(Error.ND_INV_PRID.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.ND_INV_PRID.getMessage(), actual.getErrorMessage());
    }

    @Test
    public void testStatusCheckReturnedLongPayRequestId() throws Exception {
        PayGatePaymentService paymentRequestService = new PayGatePaymentService(settings);

        TransactionStatusRequest paymentStatus = new TransactionStatusRequest("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9-90",
                "PayGateTest"
        );
        NotifyResponse actual = paymentRequestService.paymentStatusCheck(paymentStatus);


        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getHttpStatus());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertNotNull(actual.getStatus());
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(Error.DATA_PAY_REQ_ID.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PAY_REQ_ID.getMessage(), actual.getErrorMessage());
    }

    @Test
    public void testStatusCheckReturnedNoTransData() throws Exception {
        PayGatePaymentService payGatePaymentService = new PayGatePaymentService(settings);

        TransactionStatusRequest paymentStatus = new TransactionStatusRequest("529AB366-8A3F-8580-D2C7-A93CF00294FB",
                "aseyuvg43ntH"
        );

        NotifyResponse actual = payGatePaymentService.paymentStatusCheck(paymentStatus);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getHttpStatus());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertNotNull(actual.getStatus());
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertNotNull(actual.getError());
        Assert.assertEquals(Error.NO_TRANS_DATA.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.NO_TRANS_DATA.getMessage(), actual.getErrorMessage());
    }

    @Test
    public void testErrorStatusCheckInvalidChecksum() throws Exception {
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_CHK",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32), Locale.English,
                Country.South_Africa, "customer@paygate.co.za"
        );

        PaymentResponse initiate = paymentRequestService.paymentRequest(request);
        settings.setEncryptionKey("Wrong key.");
        TransactionStatusRequest statusCheck = new TransactionStatusRequest(initiate.getPayRequestId(),
                "PayGateTest-INV_CHK"
        );

        NotifyResponse actual = paymentRequestService.paymentStatusCheck(statusCheck);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_CHK.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_CHK.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testRequestsStatusCheckInvalidPayGateId() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_PGID",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32), Locale.English,
                Country.South_Africa, "customer@paygate.co.za"
        );


        PaymentResponse initial = paymentRequestService.paymentRequest(request);
        TransactionStatusRequest statusCheck = new TransactionStatusRequest(initial.getPayRequestId(),
                "PayGateTest-INV_PGID"
        );

        settings.setPayGateId(10011072131L);
        NotifyResponse actual = paymentRequestService.paymentStatusCheck(statusCheck);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_CHK.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_CHK.getMessage(), actual.getErrorMessage());
    }


    @Test
    public void testRequests() throws Exception {
        TransactionStatusRequest statusCheck = new TransactionStatusRequest("4433C443-C90F-0736-BF22-0E43584C8FE3",
                "ZAFN60WP1EA"
        );

        NotifyResponse actual = paymentRequestService.paymentStatusCheck(statusCheck);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.SUCCESS, actual.getStatus());
        Assert.assertEquals(settings.getPayGateId(), actual.getPayGateId());
        Assert.assertEquals("ZAFN60WP1EA", actual.getReference());
        Assert.assertNull(actual.getErrorCode());
        Assert.assertNull(actual.getErrorMessage());
    }
}

