package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.*;
import org.junit.Assert;
import org.junit.Test;

public class NotifyResponseTest {


    @Test
    public void testConstructor() {
        NotifyResponse notifyResponse = new NotifyResponse(
                2L,
                "ab45",
                "efg12345",
                TransactionStatus.APPROVED,
                PaymentResult.AUTH_DONE,
                "ie456bc",
                Currency.U_S_Dollar,
                10L,
                "this is a test",
                1L,
                "AX",
                PaymentMethod.BANK_TRANSFER,
                "700"
        );

        Assert.assertNotNull(notifyResponse.getPayGateId());
        Assert.assertNotNull(notifyResponse.getPayRequestId());
        Assert.assertNotNull(notifyResponse.getReference());
        Assert.assertNotNull(notifyResponse.getTransactionStatus());
        Assert.assertNotNull(notifyResponse.getResultCode());
        Assert.assertNotNull(notifyResponse.getChecksum());
        Assert.assertNotNull(notifyResponse.getResultDesc());
        Assert.assertNotNull(notifyResponse.getAuthCode());
        Assert.assertNotNull(notifyResponse.getCurrency());
        Assert.assertNotNull(notifyResponse.getAmount());
        Assert.assertNotNull(notifyResponse.getTransactionId());
        Assert.assertNotNull(notifyResponse.getRiskIndicator());
        Assert.assertNotNull(notifyResponse.getPayMethod());

        Assert.assertNull(notifyResponse.getPayMethodDetail());
        Assert.assertNull(notifyResponse.getUser1());
        Assert.assertNull(notifyResponse.getUser2());
        Assert.assertNull(notifyResponse.getUser3());
        Assert.assertNull(notifyResponse.getPayVaultData1());
        Assert.assertNull(notifyResponse.getPayVaultData2());

    }

    @Test
    public void testEqualsTrue() {

        NotifyResponse actual = new NotifyResponse(2L, "ab45", "efg12345", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "efg123", Currency.U_S_Dollar, 100L,
                "this is a test 2", 1L, "AX", PaymentMethod.CREDIT_CARD, "700"
        );
        NotifyResponse expected = new NotifyResponse(2L, "ab45", "efg12345", TransactionStatus.CANCELLED, PaymentResult.AUTH_DECLINED, "ie456bc", Currency.Australian_Dollar, 10L,
                "this is a test", 1L, "AX", PaymentMethod.BANK_TRANSFER, "800"
        );

        //optional fields
        actual.setPayMethodDetail("this is a EFT transaction");
        actual.setUser1("Ish");
        actual.setUser2("Emma");
        actual.setUser3("marry");
        actual.setVaultId("re12");
        actual.setPayVaultData1("this is a vault");
        actual.setPayVaultData1("this is a vault 2");
        //optional fields
        expected.setUser1("Ismail");
        expected.setUser2("Eddy");
        expected.setPayMethodDetail("this is a Credit card transaction");
        expected.setUser3("Lorry");
        expected.setVaultId("ff12");
        expected.setPayVaultData1("this is a vault test");
        expected.setPayVaultData1("this is a vault test 2");

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testNotEqualPayGateIdNotSame() {

        NotifyResponse notifyResponseOne = new NotifyResponse(2L, "ab45", "efg12345", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "ie456bc", Currency.Australian_Dollar, 10L,
                "this is a test", 1L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );
        NotifyResponse notifyResponseTwo = new NotifyResponse(3L, "ab45", "efg12345", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "ie456bc", Currency.Australian_Dollar, 10L,
                "this is a test", 1L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );

        //optional fields
        notifyResponseOne.setUser1("Ismail");
        notifyResponseOne.setUser2("Eddy");
        notifyResponseOne.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseOne.setUser3("Lorry");
        notifyResponseOne.setVaultId("ff12");
        notifyResponseOne.setPayVaultData1("this is a vault test");
        notifyResponseOne.setPayVaultData1("this is a vault test 2");

        //optional fields
        notifyResponseTwo.setUser1("Ismail");
        notifyResponseTwo.setUser2("Eddy");
        notifyResponseTwo.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseTwo.setUser3("Lorry");
        notifyResponseTwo.setVaultId("ff12");
        notifyResponseTwo.setPayVaultData1("this is a vault test");
        notifyResponseTwo.setPayVaultData1("this is a vault test 2");

        Assert.assertNotEquals(notifyResponseOne.getPayGateId(), notifyResponseTwo.getPayGateId());
        Assert.assertNotEquals(notifyResponseOne, notifyResponseTwo);
    }

    @Test
    public void testNotEqualReferenceNotSame() {
        NotifyResponse notifyResponseOne = new NotifyResponse(2L, "ab45", "efg12345", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "ie456bc", Currency.Australian_Dollar, 10L,
                "this is a test", 1L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );
        NotifyResponse notifyResponseTwo = new NotifyResponse(2L, "ab45eg", "efg123454", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "ie456bc", Currency.Australian_Dollar,
                10L, "this is a test", 1L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );

        //optional fields
        notifyResponseOne.setUser1("Ismail");
        notifyResponseOne.setUser2("Eddy");
        notifyResponseOne.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseOne.setUser3("Lorry");
        notifyResponseOne.setVaultId("ff12");
        notifyResponseOne.setPayVaultData1("this is a vault test");
        notifyResponseOne.setPayVaultData1("this is a vault test 2");
        //optional fields
        notifyResponseTwo.setUser1("Ismail");
        notifyResponseTwo.setUser2("Eddy");
        notifyResponseTwo.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseTwo.setUser3("Lorry");
        notifyResponseTwo.setVaultId("ff12");
        notifyResponseTwo.setPayVaultData1("this is a vault test");
        notifyResponseTwo.setPayVaultData1("this is a vault test 2");

        Assert.assertNotEquals(notifyResponseOne.getReference(), notifyResponseTwo.getReference());
        Assert.assertNotEquals(notifyResponseOne, notifyResponseTwo);


    }

    @Test
    public void testNotEqualTransactionIdNotSame() {

        NotifyResponse notifyResponseOne = new NotifyResponse(2L, "ab45", "efg12345", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "ie456bc", Currency.Australian_Dollar, 10L,
                "this is a test", 5L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );
        NotifyResponse notifyResponseTwo = new NotifyResponse(5L, "ab45e", "efg123456", TransactionStatus.DECLINED, PaymentResult.AUTH_DECLINED, "ie456bc", Currency.U_S_Dollar, 15L,
                "this is a test 2", 1L, "AX", PaymentMethod.BANK_TRANSFER, "700"
        );

        //optional fields
        notifyResponseOne.setUser1("Ismail");
        notifyResponseOne.setUser2("Eddy");
        notifyResponseOne.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseOne.setUser3("Lorry");
        notifyResponseOne.setVaultId("ff12");
        notifyResponseOne.setPayVaultData1("this is a vault test");
        notifyResponseOne.setPayVaultData1("this is a vault test 2");
        //optional fields
        notifyResponseTwo.setUser1("Ismail");
        notifyResponseTwo.setUser2("Eddy");
        notifyResponseTwo.setPayMethodDetail("this is a Credit card transaction");
        notifyResponseTwo.setUser3("Lorry");
        notifyResponseTwo.setVaultId("ff12");
        notifyResponseTwo.setPayVaultData1("this is a vault test");
        notifyResponseTwo.setPayVaultData1("this is a vault test 2");

        Assert.assertNotEquals(notifyResponseOne.getTransactionId(), notifyResponseTwo.getTransactionId());
        Assert.assertNotEquals(notifyResponseOne, notifyResponseTwo);
    }

    @Test
    public void testCalculateChecksum() throws Exception {

        String key = "secret";

        NotifyResponse notifyResponse = new NotifyResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                "PayGate Test", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "P3TPSQ", Currency.Rand, 3299L,
                "Auth Done", 36645089L, "AX", PaymentMethod.CREDIT_CARD,
                "53e1561ed2b98db6221b3f0c387a0770"
        );

        notifyResponse.setPayMethodDetail("Visa");
        Assert.assertEquals(notifyResponse.getChecksum(), notifyResponse.calculateChecksum(key));
    }

    @Test
    public void testVerifyChecksumTrue() throws Exception {

        String key = "secret";
        NotifyResponse notifyResponse = new NotifyResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                "PayGate Test", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "P3TPSQ", Currency.Rand, 3299L,
                "Auth Done", 36645089L, "AX", PaymentMethod.CREDIT_CARD,
                "56310d6fdab5561cce43620842c63dd6"
        );

        notifyResponse.setPayMethodDetail("Visa");
        notifyResponse.setUser1("SpecialKey");
        Assert.assertTrue(notifyResponse.verifyChecksum(key));
    }

    @Test
    public void testVerifyChecksumFalse() throws Exception {

        String key = "secret";
        NotifyResponse notifyResponse = new NotifyResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                "PayGate Test", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "P3TPSQ", Currency.Rand, 3299L,
                "Auth Done", 36645089L, "AX", PaymentMethod.CREDIT_CARD,
                "53e1561ed2b98db6221b3f0c387a0771"
        );


        Assert.assertFalse(notifyResponse.verifyChecksum(key));
    }

    @Test
    public void testUpdateCheckSumMethod() throws Exception {
        String key = "secret";

        NotifyResponse notifyResponse = new NotifyResponse(10011072130L, "26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                "PayGate Test", TransactionStatus.APPROVED, PaymentResult.AUTH_DONE, "P3TPSQ", Currency.Rand, 3299L,
                "Auth Done", 36645089L, "AX", PaymentMethod.CREDIT_CARD, ""
        );
        notifyResponse.updateChecksum(key);

        Assert.assertNotEquals("", notifyResponse.getChecksum());
    }
}
