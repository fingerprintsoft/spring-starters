package co.fingerprintsoft.payment.paygate.domain;


import co.fingerprintsoft.payment.paygate.Country;
import co.fingerprintsoft.payment.paygate.Currency;
import co.fingerprintsoft.payment.paygate.Locale;
import co.fingerprintsoft.payment.paygate.api.PayGateSettings;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class PaymentRequestTest {

    @Test
    public void testConstructor() {
        PaymentRequest request = new PaymentRequest(
                "fbc1234",
                10L,
                Currency.U_S_Dollar,
                LocalDateTime.of(2016, 12, 02, 2, 30, 22),
                Locale.Arabic_Saudi_Arabia,
                Country.Saudi_Arabia,
                "sahuc@gmail.com"
        );

        //required fields
        Assert.assertNotNull(request.getReference());
        Assert.assertNotNull(request.getAmount());
        Assert.assertNotNull(request.getCurrency());
        Assert.assertNotNull(request.getLocale());
        Assert.assertNotNull(request.getCountry());
        Assert.assertNotNull(request.getTransactionDate());
        Assert.assertNotNull(request.getEmail());

        //optional fields
        Assert.assertFalse(request.getPayMethod().isPresent());
        Assert.assertFalse(request.getPayMethodDetail().isPresent());
        Assert.assertFalse(request.getUser1().isPresent());
        Assert.assertFalse(request.getUser2().isPresent());
        Assert.assertFalse(request.getUser3().isPresent());
        Assert.assertFalse(request.getVault().isPresent());
        Assert.assertFalse(request.getVaultId().isPresent());

    }

    @Test
    public void testEqualTrue() {

        PaymentRequest actual = new PaymentRequest(
                "fbc1234",
                10L,
                Currency.U_S_Dollar,
                LocalDateTime.of(2016, 12, 02, 2, 30, 22),
                Locale.Arabic_Saudi_Arabia,
                Country.Saudi_Arabia,
                "sahuc@gmail.com"
        );
        PaymentRequest expected = new PaymentRequest(
                "fbc1234",
                10L,
                Currency.Rand,
                LocalDateTime.of(2016, 12, 02, 2, 30, 22),
                Locale.English,
                Country.South_Africa,
                "sahuc@yahoo.com"
        );

        //optional fields
        actual.setPayMethod("EFT");
        actual.setPayMethodDetail("this a eft transaction");
        actual.setUser1("George");
        actual.setUser2("Hazel");
        actual.setUser3("Piet");
        actual.setVault(1);
        actual.setVaultId("qft234");

        //optional fields
        expected.setPayMethod("Credit card");
        expected.setPayMethodDetail("this a credit card transaction");
        expected.setUser1("Peter");
        expected.setUser2("anna");
        expected.setUser3("mary");
        expected.setVault(2);
        expected.setVaultId("wpr234");


        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testNotEqualsReferenceNotSame() {
        PaymentRequest requestOne = new PaymentRequest(
                "abd345",
                10L,
                Currency.U_S_Dollar,
                LocalDateTime.of(2016, 12, 02, 2, 30, 22),
                Locale.Arabic_Saudi_Arabia,
                Country.Saudi_Arabia,
                "sahuc@gmail.com"
        );
        PaymentRequest requestTwo = new PaymentRequest(
                "fbc1234", 10L,
                Currency.Rand,
                LocalDateTime.of(2016, 12, 02, 2, 30, 22),
                Locale.English,
                Country.South_Africa,
                "sahuc@yahoo.com"
        );

        //optional fields
        requestOne.setPayMethod("Credit card");
        requestOne.setUser1("Peter");
        requestOne.setPayMethodDetail("this a Credit card transaction");
        requestOne.setUser2("anna");
        requestOne.setUser3("mary");
        requestOne.setVault(2);
        requestOne.setVaultId("wpr234");
        //optional fields
        requestTwo.setPayMethod("EFT");
        requestTwo.setPayMethodDetail("this a EFT transaction");
        requestTwo.setUser1("Peter");
        requestTwo.setUser2("anna");
        requestTwo.setUser3("mary");
        requestTwo.setVault(2);
        requestTwo.setVaultId("wpr234e");

        Assert.assertNotEquals(requestOne.getReference(), requestTwo.getReference());
        Assert.assertNotEquals(requestOne, requestTwo);
    }

    @Test
    public void testCalculateCheckSumMethod() throws Exception {
        PayGateSettings settings = new PayGateSettings(10011072130L, "secret", "https://www.paygate.co.za/thankyou", null);
        PaymentRequest requestOne = new PaymentRequest(
                "PayGate Test",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 10, 10, 49, 16),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );
        Assert.assertEquals("0bcaea6fa6bc0337e066db9826088557", requestOne.calculateChecksum(settings));
    }

}
