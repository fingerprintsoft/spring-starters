package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.TransactionStatus;
import org.junit.Assert;
import org.junit.Test;

public class PaymentStatusTest {

    @Test
    public void testConstructor() {
        PaymentStatus paymentStatusCheck = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "800");

        Assert.assertNotNull(paymentStatusCheck.getPayRequestId());
        Assert.assertNotNull(paymentStatusCheck.getTransactionStatus());
        Assert.assertNotNull(paymentStatusCheck.getChecksum());
    }

    @Test
    public void testEqualsTrue() {
        PaymentStatus actual = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                TransactionStatus.DECLINED,
                "PayGate Test",
                "800");
        PaymentStatus expected = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "700");

        Assert.assertEquals(actual,expected);
    }

    @Test
    public void testNotEqualsPayRequestIdNotSame (){
        PaymentStatus statusCheckOne = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "800");
        PaymentStatus statusCheckTwo = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC8",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "700");

        Assert.assertNotEquals(statusCheckOne.getPayRequestId(),statusCheckTwo.getPayRequestId());
        Assert.assertNotEquals(statusCheckOne,statusCheckTwo);


    }

    @Test
    public void testCalculateChecksum() throws Exception {
        String key ="secret";
        Long payGateId =10011072130L;
        PaymentStatus statusCheckOne = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "2fae4c5cde9ac8ed70f769c3ff843d72");

        Assert.assertEquals(statusCheckOne.getChecksum(),statusCheckOne.calculateChecksum(key,payGateId));
    }

    @Test
    public void testVerifyCheckSumTrue() throws Exception {
        String key ="secret";
        Long payGateId =10011072130L;
        PaymentStatus statusCheckOne = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "2fae4c5cde9ac8ed70f769c3ff843d72");

        Assert.assertTrue(statusCheckOne.verifyChecksum(key,payGateId));
    }

    @Test
    public void testVerifyCheckSumFalse() throws Exception {
        String key ="secret";
        Long payGateId =10011072130L;
        PaymentStatus statusCheckOne = new PaymentStatus("26F1EE9D-FB68-D6C2-5D36-ADA8C5F88BC9",
                TransactionStatus.APPROVED,
                "PayGate Test",
                "2fae4c5cde9ac8ed70f769c3ff843d73");

        Assert.assertFalse(statusCheckOne.verifyChecksum(key,payGateId));
    }
}
