package co.fingerprintsoft.payment.paygate.api;

import co.fingerprintsoft.payment.paygate.Country;
import co.fingerprintsoft.payment.paygate.Currency;
import co.fingerprintsoft.payment.paygate.Locale;
import co.fingerprintsoft.payment.paygate.domain.Error;
import co.fingerprintsoft.payment.paygate.domain.PaymentRequest;
import co.fingerprintsoft.payment.paygate.domain.PaymentResponse;
import co.fingerprintsoft.payment.paygate.domain.Status;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

@RunWith(JMockit.class)
public class PayGatePaymentServiceTest {

    private PayGatePaymentService paymentRequestService;
    private PayGateSettings settings;
    private static final Logger LOGGER = LoggerFactory.getLogger(PayGatePaymentServiceTest.class);

    @Before
    public void setup() {
        settings = new PayGateSettings(10011072130L, "secret", "https://www.paygate.co.za/thankyou", null);
        paymentRequestService = new PayGatePaymentService(settings);
    }

    @Test
    public void testRequests() throws Exception {
        PaymentResponse expected = new PaymentResponse(settings.getPayGateId(), "", "PayGateTest", "");
        PaymentRequest request = new PaymentRequest(
                "PayGateTest",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32), Locale.English,
                Country.South_Africa, "customer@paygate.co.za"
        );


        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.SUCCESS, actual.getStatus());
        Assert.assertTrue(actual.verifyChecksum(settings.getEncryptionKey()));
        Assert.assertEquals(expected.getPayGateId(), actual.getPayGateId());
        Assert.assertEquals(expected.getReference(), actual.getReference());
        Assert.assertNull(actual.getErrorCode());
        Assert.assertNull(actual.getErrorMessage());
    }

    @Test
    public void testRequestsReturnedInvalidPayGateId(@Mocked PayGateSettings mockSettings) throws Exception {
        new Expectations() {
            {
                mockSettings.getEncryptionKey();
                result = "secret";
                mockSettings.getReturnUrl();
                result = "https://www.paygate.co.za/thankyou";
                mockSettings.getNotifyUrl();
                result = null;
                mockSettings.getPayGateId();
                result = 10011072130L;
                result = 10011072130L;
                result = 10011072130L;
                result = 100110721302345L;
            }};

        PayGatePaymentService paymentRequestService = new PayGatePaymentService(mockSettings);

        PaymentResponse expected = new PaymentResponse(this.settings.getPayGateId(), "", "PayGateTest-INV_PGID_RET", "");
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_PGID_RET",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32), Locale.English,
                Country.South_Africa, "customer@paygate.co.za"
        );


        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.VERIFICATION_FAILURE, actual.getStatus());
        Assert.assertTrue(actual.verifyChecksum(settings.getEncryptionKey()));
        Assert.assertEquals(expected.getPayGateId(), actual.getPayGateId());
        Assert.assertEquals(expected.getReference(), actual.getReference());
        Assert.assertEquals("PGID_MOD", actual.getErrorCode());
        Assert.assertEquals("The PayGate ID being used to post data to PayGate has been modified during response.", actual.getErrorMessage());

    }

    @Test
    public void testRequestsAllOptionalFields() throws Exception {

        PaymentResponse expected = new PaymentResponse(settings.getPayGateId(), "", "PayGateTest-OPT_FL", "");
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-OPT_FL",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32), Locale.English,
                Country.South_Africa, "customer@paygate.co.za"
        );

        request.setPayMethod("CC");
        request.setPayMethodDetail("Visa");
        settings.setNotifyUrl("https://www.paygate.co.za/notify");
        request.setUser1("SpecialKey");
        request.setUser2("SpecialKey2");
        request.setUser3("SpecialKey3");
        request.setVault(1);
        PaymentResponse actual  = paymentRequestService.paymentRequest(request);

        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.SUCCESS, actual.getStatus());
        Assert.assertTrue(actual.verifyChecksum(settings.getEncryptionKey()));
        Assert.assertEquals(expected.getPayGateId(), actual.getPayGateId());
        Assert.assertEquals(expected.getReference(), actual.getReference());
        Assert.assertNull(actual.getErrorCode());
        Assert.assertNull(actual.getErrorMessage());
    }

    @Test
    public void testErrorResponseInvalidChecksum() throws Exception {

        settings.setEncryptionKey("Wrong key.");
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_CHK",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_CHK.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_CHK.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidPayGateId() throws Exception {

        settings.setPayGateId(1L);
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_PGID",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_CHK.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_CHK.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseMissingReference() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_PW.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PW.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidAmount() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_AMT",
                -3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_AMT_NUM.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_AMT_NUM.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseMissingCurrency(@Mocked final Currency mock) throws Exception {
        new Expectations() {
            {
                mock.getCurrencyCode();
                result = "";
            }};

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-MISS_CURR",
                3299L,
                mock,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_PW.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PW.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidCurrency(@Mocked final Currency mockCurrency) throws Exception {
        new Expectations() {
            {
                mockCurrency.getCurrencyCode();
                result = "LLLL";
            }};

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_CURR",
                3299L,
                mockCurrency,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.PGID_NOT_EN.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.PGID_NOT_EN.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidReturnUrl() throws Exception {

        settings.setReturnUrl(null);
        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_RET_URL",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_PW.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PW.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidLocale(@Mocked Locale mockLocale) throws Exception {
        new Expectations() {
            {
                mockLocale.getCode();
                result = "LLLL";
            }};

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_LOC",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                mockLocale,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.LOCALE_INVALID.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.LOCALE_INVALID.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidCountry(@Mocked Country mockCountry) throws Exception {
        new Expectations() {
            {
                mockCountry.getCountryCode();
                result = "LLLL";
            }};

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_CNT",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                mockCountry,
                "customer@paygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.CNTRY_INVALID.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.CNTRY_INVALID.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidEmail() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_EML",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customerpaygate.co.za"
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.INV_EMAIL.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.INV_EMAIL.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseMissingEmail() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-MIS_EML",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                ""
        );

        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
        Assert.assertEquals(Status.SUCCESS, actual.getStatus());
        Assert.assertTrue(actual.verifyChecksum(settings.getEncryptionKey()));
        Assert.assertNull(actual.getErrorCode());
        Assert.assertNull(actual.getErrorMessage());

    }

    @Test
    public void testErrorResponseInvalidPayMethod() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_PM",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        request.setPayMethod("CCnhji");
        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_PM.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PM.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidPayMethodDetails() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_PMD",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        request.setPayMethodDetail("Visa134");
        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.DATA_PM.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.DATA_PM.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

    @Test
    public void testErrorResponseInvalidVault() throws Exception {

        PaymentRequest request = new PaymentRequest(
                "PayGateTest-INV_VLT",
                3299L,
                Currency.Rand,
                LocalDateTime.of(2016, 03, 24, 14, 45, 32),
                Locale.English,
                Country.South_Africa,
                "customer@paygate.co.za"
        );

        request.setVault(5);
        PaymentResponse actual = paymentRequestService.paymentRequest(request);
        Assert.assertNotNull(actual);
        Assert.assertEquals(Status.FAILURE, actual.getStatus());
        Assert.assertEquals(Error.INVALID_VAULT.getCode(), actual.getErrorCode());
        Assert.assertEquals(Error.INVALID_VAULT.getMessage(), actual.getErrorMessage());
        Assert.assertEquals(Integer.valueOf(200), actual.getHttpStatus());
    }

}
