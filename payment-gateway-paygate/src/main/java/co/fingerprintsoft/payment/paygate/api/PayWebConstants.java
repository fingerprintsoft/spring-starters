package co.fingerprintsoft.payment.paygate.api;

public class PayWebConstants {
    public static final String QUERY = "https://secure.paygate.co.za/payweb3/query.trans";
    public static final String INITIATE = "https://secure.paygate.co.za/payweb3/initiate.trans";
    public static final String REDIRECT = "https://secure.paygate.co.za/payweb3/process.trans";
}
