package co.fingerprintsoft.payment.paygate.domain;

public enum Status {

    FAILURE, VERIFICATION_FAILURE, SUCCESS

}
