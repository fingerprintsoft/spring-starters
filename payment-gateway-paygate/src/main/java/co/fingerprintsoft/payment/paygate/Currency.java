package co.fingerprintsoft.payment.paygate;

import java.util.HashMap;
import java.util.Map;

public enum Currency {

    Afghani("Afghani", "AFA"),
    Lek("Lek", "ALL"),
    Algerian_Dinar("Algerian Dinar", "DZD"),
    U_S_Dollar("U.S. Dollar", "USD"),
    Euro("Euro", "EUR"),
    Kwanza("Kwanza", "AOA"),
    E_Caribbean_Dollar("E. Caribbean Dollar", "XCD"),
    Norwegian_Krone("Norwegian Krone", "NOK"),
    Argentine_Peso("Argentine Peso", "ARS"),
    Armenian_Dram("Armenian Dram", "AMD"),
    Aruban_Guilder("Aruban Guilder", "AWG"),
    Australian_Dollar("Australian Dollar", "AUD"),
    Azerbaijan_Manat("Azerbaijan Manat", "AZM"),
    Bahamian_Dollar("Bahamian Dollar", "BSD"),
    Bahraini_Dinar("Bahraini Dinar", "BHD"),
    Taka("Taka", "BDT"),
    Barbados_Dollar("Barbados Dollar", "BBD"),
    Belarussian_Ruble("Belarussian Ruble", "BYR"),
    Belize_Dollar("Belize Dollar", "BZD"),
    CFA_Franc_BCEAO("CFA Franc BCEAO", "XOF"),
    Bermudian_Dollar("Bermudian Dollar", "BMD"),
    Indian_Rupee("Indian Rupee", "INR"),
    Boliviano("Boliviano", "BOB"),
    Bosnian_Convertible_Mark("Bosnian Convertible Mark", "BAM"),
    Pula("Pula", "BWP"),
    Brazilian_Real("Brazilian Real", "BRL"),
    Brunei_Dollar("Brunei Dollar", "BND"),
    Bulgarian_Lev("Bulgarian Lev", "BGN"),
    Burundi_Franc("Burundi Franc", "BIF"),
    Riel("Riel", "KHR"),
    CFA_Franc_BEAC("CFA Franc BEAC", "XAF"),
    Canadian_Dollar("Canadian Dollar", "CAD"),
    Cape_Verde_Escudo("Cape Verde Escudo", "CVE"),
    Cayman_Is_Dollar("Cayman Is. Dollar", "KYD"),
    Chilean_Peso("Chilean Peso", "CLP"),
    Yuan_Renminbi("Yuan Renminbi", "CNY"),
    Colombian_Peso("Colombian Peso", "COP"),
    Comoro_Franc("Comoro Franc", "KMF"),
    New_Zealand_Dollar("New Zealand Dollar", "NZD"),
    Costa_Rican_Colon("Costa Rican Colon", "CRC"),
    Croatian_Kuna("Croatian Kuna", "HRK"),
    Cuban_Peso("Cuban Peso", "CUP"),
    Cyprus_Pound("Cyprus Pound", "CYP"),
    Czech_Koruna("Czech Koruna", "CZK"),
    Franc_Congolais__formerly_New_Zaire("Franc Congolais (formerly New Zaire)", "CDF"),
    Danish_Krone("Danish Krone", "DKK"),
    Djibouti_Franc("Djibouti Franc", "DJF"),
    Dominican_Peso("Dominican Peso", "DOP"),
    Timor_Escudo("Timor Escudo", "TPE"),
    Sucre("Sucre", "ECS"),
    Egyptian_Pound("Egyptian Pound", "EGP"),
    Eritean_Nakfa("Eritean Nakfa", "ERN"),
    Kroon("Kroon", "EEK"),
    Ethiopian_Birr("Ethiopian Birr", "ETB"),
    European_Currency_Unit("European Currency Unit", "XEU"),
    Falkland_Is_Pound("Falkland Is. Pound", "FKP"),
    Fiji_Dollar("Fiji Dollar", "FJD"),
    CFP_Franc("CFP Franc", "XPF"),
    Dalasi("Dalasi", "GMD"),
    Georgian_Lari("Georgian Lari", "GEL"),
    Deutsche_Mark("Deutsche Mark", "DEM"),
    Cedi("Cedi", "GHC"),
    Gibraltar_Pound("Gibraltar Pound", "GIP"),
    Quetzal("Quetzal", "GTQ"),
    Guinea_Franc("Guinea Franc", "GNF"),
    Guinea_Bissau_Peso("Guinea-Bissau Peso", "GWP"),
    Guyana_Dollar("Guyana Dollar", "GYD"),
    Gourde("Gourde", "HTG"),
    Lempira("Lempira", "HNL"),
    Hong_Kong_Dollar("Hong Kong Dollar", "HKD"),
    Forint("Forint", "HUF"),
    Iceland_Krona("Iceland Krona", "ISK"),
    Rupiah("Rupiah", "IDR"),
    Iranian_Airline_Rate("Iranian Airline Rate", "IRA"),
    Iranian_Rial("Iranian Rial", "IRR"),
    Iraqi_Dinar("Iraqi Dinar", "IQD"),
    New_Israeli_Shekel("New Israeli Shekel", "ILS"),
    Jamaican_Dollar("Jamaican Dollar", "JMD"),
    Yen("Yen", "JPY"),
    Jordanian_Dinar("Jordanian Dinar", "JOD"),
    Tenge("Tenge", "KZT"),
    Kenyan_Shilling("Kenyan Shilling", "KES"),
    North_Korean_Won("North Korean Won", "KPW"),
    Won("Won", "KRW"),
    Kuwaiti_Dinar("Kuwaiti Dinar", "KWD"),
    Som("Som", "KGS"),
    Kip("Kip", "LAK"),
    Latvian_Lats("Latvian Lats", "LVL"),
    Lebanese_Pound("Lebanese Pound", "LBP"),
    Rand("Rand", "ZAR"),
    Liberian_Dollar("Liberian Dollar", "LRD"),
    Libyan_Dinar("Libyan Dinar", "LYD"),
    Swiss_Franc("Swiss Franc", "CHF"),
    Lithuanian_Litas("Lithuanian Litas", "LTL"),
    Pataca("Pataca", "MOP"),
    Denar("Denar", "MKD"),
    Malagasy_Franc("Malagasy Franc", "MGF"),
    Malawi_Kwacha("Malawi Kwacha", "MWK"),
    Malaysian_Ringgit("Malaysian Ringgit", "MYR"),
    Rufiyaa("Rufiyaa", "MVR"),
    Maltese_Lira("Maltese Lira", "MTL"),
    Ouguiya("Ouguiya", "MRO"),
    Mauritius_Rupee("Mauritius Rupee", "MUR"),
    Mexican_Peso("Mexican Peso", "MXN"),
    Moldovan_Leu("Moldovan Leu", "MDL"),
    Tugrik("Tugrik", "MNT"),
    Yugoslavian_New_Dinar("Yugoslavian New Dinar", "YUM"),
    Moroccan_Dirham("Moroccan Dirham", "MAD"),
    Metical("Metical", "MZM"),
    Kyat("Kyat", "MMK"),
    Namibia_Dollar("Namibia Dollar", "NAD"),
    Nepalese_Rupee("Nepalese Rupee", "NPR"),
    Nether_Antillian_Guilder("Nether. Antillian Guilder", "ANG"),
    Cordoba_Oro("Cordoba Oro", "NIO"),
    Naira("Naira", "NGN"),
    Rial_Omani("Rial Omani", "OMR"),
    Pakistan_Rupee("Pakistan Rupee", "PKR"),
    Balboa("Balboa", "PAB"),
    Kina("Kina", "PGK"),
    Guarani("Guarani", "PYG"),
    Nuevo_Sol("Nuevo Sol", "PEN"),
    Philippine_Peso("Philippine Peso", "PHP"),
    Polish_New_Zloty("Polish New Zloty", "PLN"),
    Qatari_Rial("Qatari Rial", "QAR"),
    Leu("Leu", "ROL"),
    Russian_Ruble__International("Russian Ruble (International)", "RUB"),
    Russian_Ruble__Domestic("Russian Ruble (Domestic)", "RUR"),
    Rwanda_Franc("Rwanda Franc", "RWF"),
    Tala("Tala", "WST"),
    Dobra("Dobra", "STD"),
    Saudi_Riyal("Saudi Riyal", "SAR"),
    Seychelles_Rupee("Seychelles Rupee", "SCR"),
    Leone("Leone", "SLL"),
    Singapore_Dollar("Singapore Dollar", "SGD"),
    Slovak_Koruna("Slovak Koruna", "SKK"),
    Tolar("Tolar", "SIT"),
    Pound_Sterling("Pound Sterling", "GBP"),
    Solomon_Is_Dollar("Solomon Is. Dollar", "SBD"),
    Somali_Shilling("Somali Shilling", "SOS"),
    Sri_Lanka_Rupee("Sri Lanka Rupee", "LKR"),
    St_Helena_Pound("St. Helena Pound", "SHP"),
    Sudanese_Pound("Sudanese Pound", "SDD"),
    Sudan_Airline_Rate("Sudan Airline Rate", "SDA"),
    Surinam_Guilder("Surinam Guilder", "SRG"),
    Lilangeni("Lilangeni", "SZL"),
    Swedish_Krona("Swedish Krona", "SEK"),
    Syrian_Pound("Syrian Pound", "SYP"),
    New_Taiwan_Dollar("New Taiwan Dollar", "TWD"),
    Somoni("Somoni", "TJS"),
    Tanzanian_Shilling("Tanzanian Shilling", "TZS"),
    Thailand_Baht("Thailand Baht", "THB"),
    Paanga("Pa’anga", "TOP"),
    Trinidad_and_Tobago_Dollar("Trinidad and Tobago Dollar", "TTD"),
    Tunisian_Dinar("Tunisian Dinar", "TND"),
    Turkish_Lira("Turkish Lira", "TRL"),
    Manat("Manat", "TMM"),
    Uganda_Shilling("Uganda Shilling", "UGX"),
    Ukrainian_Hryvnia("Ukrainian Hryvnia", "UAH"),
    U_A_E_Dirham("U.A.E. Dirham", "AED"),
    Peso_Uruguayo("Peso Uruguayo", "UYU"),
    Uzbekistan_Sum("Uzbekistan Sum", "UZS"),
    Vatu("Vatu", "VUV"),
    Bolivar("Bolivar", "VEB"),
    Dong("Dong", "VND"),
    Yemeni_Rial("Yemeni Rial", "YER"),
    Zambian_Kwacha("Zambian Kwacha", "ZMK"),
    Zimbabwe_Dollar("Zimbabwe Dollar", "ZWD");

    private static final Map<String, Currency> CURR_TO_ENUM_MAP = new HashMap();
    private static final Map<String, Currency> CODE_TO_ENUM_MAP = new HashMap();

    static {
        for (Currency type : Currency.values()) {
            CURR_TO_ENUM_MAP.put(type.getCurrency(), type);
            CODE_TO_ENUM_MAP.put(type.getCurrencyCode(), type);
        }
    }

    private String currency;
    private String currencyCode;

    Currency(String currency, String currencyCode) {
        this.currency = currency;
        this.currencyCode = currencyCode;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public static Currency fromCurrencyCode(String currencyCode) {

        if (currencyCode == null) {
            return null;
        }

        Currency type = CODE_TO_ENUM_MAP.get(currencyCode);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for currencyCode " + currencyCode);
    }

    public static Currency fromCurrency(String currency) {

        if (currency == null) {
            return null;
        }

        Currency type = CURR_TO_ENUM_MAP.get(currency);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for currency " + currency);
    }
}
