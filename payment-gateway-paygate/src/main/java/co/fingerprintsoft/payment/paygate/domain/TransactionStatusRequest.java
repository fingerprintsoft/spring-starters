package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.MD5ToString;
import lombok.*;

import java.security.NoSuchAlgorithmException;

@Data
@EqualsAndHashCode(of = {"payRequestId"})
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionStatusRequest {

    @NonNull
    private String payRequestId;
    @NonNull
    private String reference;

    public String calculateChecksum(String encryptedKey,Long payGateId) throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        sb.append(payGateId);
        sb.append(payRequestId);
        sb.append(reference);
        sb.append(encryptedKey);
        return MD5ToString.md5String(sb);
    }

}
