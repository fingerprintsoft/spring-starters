package co.fingerprintsoft.payment.paygate;

import java.util.HashMap;
import java.util.Map;

public enum Locale {

    Afrikaans("Af","Afrikaans"),
    Albanian("Sq","Albanian"),
    Arabic_Saudi_Arabia("ar-sa","Arabic (Saudi Arabia)"),
    Arabic_Iraq("ar-iq","Arabic (Iraq)"),
    Arabic_Egypt("ar-eg","Arabic (Egypt)"),
    Arabic_Libya("ar-ly","Arabic (Libya)"),
    Arabic_Algeria("ar-dz","Arabic (Algeria)"),
    Arabic_Morocco("ar-ma","Arabic (Morocco)"),
    Arabic_Tunisia("ar-tn","Arabic (Tunisia)"),
    Arabic_Oman("ar-om","Arabic (Oman)"),
    Arabic_Yemen("ar-ye","Arabic (Yemen)"),
    Arabic_Syria("ar-sy","Arabic (Syria)"),
    Arabic_Jordan("ar-jo","Arabic (Jordan)"),
    Arabic_Lebanon("ar-lb","Arabic (Lebanon)"),
    Arabic_Kuwait("ar-kw","Arabic (Kuwait)"),
    Arabic_U_A_E("ar-ae","Arabic (U"),
    Arabic_Bahrain("ar-bh","Arabic (Bahrain)"),
    Arabic_Qatar("ar-qa","Arabic (Qatar)"),
    Basque("Eu","Basque"),
    Bulgarian("bg","Bulgarian"),
    Belarusian("Be","Belarusian"),
    Catalan("ca","Catalan"),
    Chinese_Taiwan("zh-tw","Chinese (Taiwan)"),
    Chinese_PRC("zh-cn","Chinese (PRC)"),
    Chinese_Hong_Kong_SAR("zh-hk","Chinese (Hong Kong SAR)"),
    Chinese_Singapore("zh-sg","Chinese (Singapore)"),
    Croatian("Hr","Croatian"),
    Czech("cs","Czech"),
    Danish("Da","Danish"),
    Dutch_Standard("nl","Dutch (Standard)"),
    Dutch_Belgium("nl-be","Dutch (Belgium)"),
    English("en","English"),
    English_United_States("en-us","English (United States)"),
    English_United_Kingdom("en-gb","English (United Kingdom)"),
    English_Australia("en-au","English (Australia)"),
    English_Canada("en-ca","English (Canada)"),
    English_New_Zealand("en-nz","English (New Zealand)"),
    English_Ireland("en-ie","English (Ireland)"),
    English_South_Africa("en-za","English (South Africa)"),
    English_Jamaica("en-jm","English (Jamaica)"),
    English_Caribbean("En","English (Caribbean)"),
    English_Belize("en-bz","English (Belize)"),
    English_Trinidad("en-tt","English (Trinidad)"),
    Estonian("et","Estonian"),
    Faeroese("fo","Faeroese"),
    Farsi("fa","Farsi"),
    Finnish("fi","Finnish"),
    French_Standard("fr","French (Standard)"),
    French_Belgium("fr-be","French (Belgium)"),
    French_Canada("fr-ca","French (Canada)"),
    French_Switzerland("fr-ch","French (Switzerland)"),
    French_Luxembourg("fr-lu","French (Luxembourg)"),
    Gaelic_Scotland("gd","Gaelic (Scotland)"),
    Irish("ga","Irish"),
    German_Standard("de","German (Standard)"),
    German_Switzerland("de-ch","German (Switzerland)"),
    German_Austria("de-at","German (Austria)"),
    German_Luxembourg("de-lu","German (Luxembourg)"),
    German_Liechtenstein("de-li","German (Liechtenstein)"),
    Greek("el","Greek"),
    Hebrew("he","Hebrew"),
    Hindi("hi","Hindi"),
    Hungarian("hu","Hungarian"),
    Icelandic("is","Icelandic"),
    Indonesian("id","Indonesian"),
    Italian_Standard("it","Italian (Standard)"),
    Italian_Switzerland("it-ch","Italian (Switzerland)"),
    Japanese("ja","Japanese"),
    Korean("ko","Korean"),
    Korean_Johab("ko","Korean (Johab)"),
    Latvian("lv","Latvian"),
    Lithuanian("lt","Lithuanian"),
    Macedonian_FYROM("mk","Macedonian (FYROM)"),
    Malaysian("ms","Malaysian"),
    Maltese("mt","Maltese"),
    Norwegian_Bokmal("no","Norwegian (Bokmal)"),
    Norwegian_Nynorsk("no","Norwegian (Nynorsk)"),
    Polish("pl","Polish"),
    Portuguese_Brazil("pt-br","Portuguese (Brazil)"),
    Portuguese_Portugal("pt","Portuguese (Portugal)"),
    Rhaeto("rm","Rhaeto-Romanic"),
    ro("Romanian","ro"),
    Romanian_Republic_of_Moldova("ro-mo","Romanian (Republic of Moldova)"),
    Russian("ru","Russian"),
    Russian_Republic_of_Moldova("ru-mo","Russian (Republic of Moldova)"),
    Sami_Lappish("sz","Sami (Lappish)"),
    Serbian_Cyrillic("sr","Serbian (Cyrillic)"),
    Serbian_Latin("sr","Serbian (Latin)"),
    Slovak("sk","Slovak"),
    Slovenian("sl","Slovenian"),
    Sorbian("sb","Sorbian"),
    Spanish_Spain("es","Spanish (Spain)"),
    Spanish_Mexico("es-mx","Spanish (Mexico)"),
    Spanish_Guatemala("es-gt","Spanish (Guatemala)"),
    Spanish_Costa_Rica("es-cr","Spanish (Costa Rica)"),
    Spanish_Panama("es-pa","Spanish (Panama)"),
    Spanish_Dominican_Republic("es-do","Spanish (Dominican Republic)"),
    Spanish_Venezuela("es-ve","Spanish (Venezuela)"),
    Spanish_Colombia("es-co","Spanish (Colombia)"),
    Spanish_Peru("es-pe","Spanish (Peru)"),
    Spanish_Argentina("es-ar","Spanish (Argentina)"),
    Spanish_Ecuador("es-ec","Spanish (Ecuador)"),
    Spanish_Chile("es-cl","Spanish (Chile)"),
    Spanish_Uruguay("es-uy","Spanish (Uruguay)"),
    Spanish_Paraguay("es-py","Spanish (Paraguay)"),
    Spanish_Bolivia("es-bo","Spanish (Bolivia)"),
    Spanish_El_Salvador("es-sv","Spanish (El Salvador)"),
    Spanish_Honduras("es-hn","Spanish (Honduras)"),
    Spanish_Nicaragua("es-ni","Spanish (Nicaragua)"),
    Spanish_Puerto_Rico("es-pr","Spanish (Puerto Rico)"),
    Sutu("sx","Sutu"),
    Swedish("sv","Swedish"),
    Swedish_Finland("sv-fi","Swedish (Finland)"),
    Thai("th","Thai"),
    Tsonga("ts","Tsonga"),
    Tswana("tn","Tswana"),
    Turkish("tr","Turkish"),
    Ukrainian("uk","Ukrainian"),
    Urdu("ur","Urdu"),
    Venda("ve","Venda"),
    Vietnamese("vi","Vietnamese"),
    Xhosa("xh","Xhosa"),
    Yiddish("ji","Yiddish"),
    Zulu("zu","Zulu");

    private static final Map<String , Locale> CODE_TO_ENUM_MAP = new HashMap();
    private static final Map<String, Locale> DESC_TO_ENUM_MAP = new HashMap();

    static {
        for (Locale type : Locale.values()) {
            CODE_TO_ENUM_MAP.put(type.getCode(), type);
            DESC_TO_ENUM_MAP.put(type.getDescription(), type);
        }
    }


    private String code;
    private String description;

    Locale(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static Locale fromDescription(String description) {

        if (description == null) {
            return null;
        }

        Locale type = DESC_TO_ENUM_MAP.get(description);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for description " + description);
    }

    public static Locale fromCode(String code) {

        if (code == null) {
            return null;
        }

        Locale type = CODE_TO_ENUM_MAP.get(code);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for code " + code);
    }
}
