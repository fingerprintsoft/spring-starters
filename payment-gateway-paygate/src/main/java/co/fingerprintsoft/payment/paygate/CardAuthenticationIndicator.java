package co.fingerprintsoft.payment.paygate;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public enum CardAuthenticationIndicator {

    NOT_AUTHENTICATED("N","Not Authenticated"),
    AUTHENTICATED("A","Authenticated", true),
    NOT_APPLICABLE("X", "Not Applicable");

    private static final Map<String, CardAuthenticationIndicator> CODE_TO_ENUM_MAP = new HashMap();
    private static final Map<String, CardAuthenticationIndicator> DESC_TO_ENUM_MAP = new HashMap();

    static {
        for (CardAuthenticationIndicator type : CardAuthenticationIndicator.values()) {
            CODE_TO_ENUM_MAP.put(type.getCode(), type);
            DESC_TO_ENUM_MAP.put(type.getDescription(), type);
        }
    }


    private String code;
    private String description;
    private boolean success;

    CardAuthenticationIndicator(String code, String description) {
        this.code = code;
        this.description = description;
    }

    CardAuthenticationIndicator(String code, String description, boolean success) {
        this.code = code;
        this.description = description;
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSuccess() {
        return success;
    }

    public static CardAuthenticationIndicator fromDescription(String description) {

        if (description == null) {
            return null;
        }

        CardAuthenticationIndicator type = DESC_TO_ENUM_MAP.get(description);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for description " + description);
    }

    public static CardAuthenticationIndicator fromCode(String code) {

        if (code == null) {
            return null;
        }

        code = StringUtils.substring(code, 0, 1);

        CardAuthenticationIndicator type = CODE_TO_ENUM_MAP.get(code);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for code " + code);
    }
}
