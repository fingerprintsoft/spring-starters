package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.MD5ToString;
import co.fingerprintsoft.payment.paygate.TransactionStatus;
import lombok.*;

import java.security.NoSuchAlgorithmException;

@Data
@EqualsAndHashCode(of = {"payRequestId"})
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaymentStatus {

    @NonNull
    private String payRequestId;

    @NonNull
    private TransactionStatus transactionStatus;

    @NonNull
    private String reference;

    @NonNull
    private String checksum;

    public String calculateChecksum(String encryptedKey,Long payGateId) throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        sb.append(payGateId);
        sb.append(payRequestId);
        sb.append(transactionStatus.getTransactionCode());
        sb.append(reference);
        sb.append(encryptedKey);
        return MD5ToString.md5String(sb);
    }

    public boolean verifyChecksum(String encryptedKey,Long payGateId) throws NoSuchAlgorithmException {
        if (this.checksum.equals(calculateChecksum(encryptedKey,payGateId))) {
            return true;
        }
        return false;
    }

}
