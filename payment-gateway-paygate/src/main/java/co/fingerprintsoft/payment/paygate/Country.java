package co.fingerprintsoft.payment.paygate;

import java.util.HashMap;
import java.util.Map;

public enum Country {

    Afghanistan("Afghanistan", "AFG"),
    Albania("Albania", "ALB"),
    Algeria("Algeria", "DZA"),
    American_Samoa("American Samoa", "ASM"),
    Andorra("Andorra", "AND"),
    Angola("Angola", "AGO"),
    Anguilla("Anguilla", "AIA"),
    Antarctica("Antarctica", "ATA"),
    Antigua_and_Barbuda("Antigua and Barbuda", "ATG"),
    Argentina("Argentina", "ARG"),
    Armenia("Armenia", "ARM"),
    Aruba("Aruba", "ABW"),
    Australia("Australia", "AUS"),
    Austria("Austria", "AUT"),
    Azerbaijan("Azerbaijan", "AZE"),
    Bahamas("Bahamas", "BHS"),
    Bahrain("Bahrain", "BHR"),
    Bangladesh("Bangladesh", "BGD"),
    Barbados("Barbados", "BRB"),
    Belarus("Belarus", "BLR"),
    Belgium("Belgium", "BEL"),
    Belize("Belize", "BLZ"),
    Benin("Benin", "BEN"),
    Bermuda("Bermuda", "BMU"),
    Bhutan("Bhutan", "BTN"),
    Bolivia("Bolivia", "BOL"),
    Bosnia_and_Herzegovina("Bosnia and Herzegovina", "BIH"),
    Botswana("Botswana", "BWA"),
    Bouvet_Is("Bouvet Is.", "BVT"),
    Brazil("Brazil", "BRA"),
    British_Indian_Ocean_Territory("British Indian Ocean Territory", "IOT"),
    British_Virgin_Is("British Virgin Is.", "VGB"),
    Brunei_Darussalam("Brunei Darussalam", "BRN"),
    Bulgaria("Bulgaria", "BGR"),
    Burkina_Faso("Burkina Faso", "BFA"),
    Burundi("Burundi", "BDI"),
    Cambodia("Cambodia", "KHM"),
    Cameroon_United_Republic_of("Cameroon United Republic of", "CMR"),
    Canada("Canada", "CAN"),
    Cape_Verde_Is("Cape Verde Is.", "CPV"),
    Cayman_Is("Cayman Is.", "CYM"),
    Central_African_Republic("Central African Republic", "CAF"),
    Chad("Chad", "TCD"),
    Chile("Chile", "CHL"),
    China("China", "CHN"),
    Christmas_Is("Christmas Is.", "CXR"),
    Cocos_Keeling_Is("Cocos (Keeling) Is.", "CCK"),
    Colombia("Colombia", "COL"),
    Comoros("Comoros", "COM"),
    Congo("Congo", "COG"),
    Cook_Is("Cook Is.", "COK"),
    Costa_Rica("Costa Rica", "CRI"),
    Côte_dIvoire_Ivory_Coast("CIV", "Côte d’Ivoire (Ivory Coast)"),
    Croatia("Croatia", "HRV"),
    Cuba("Cuba", "CUB"),
    Cyprus("Cyprus", "CYP"),
    Czech_Republic("Czech Republic", "CZE"),
    Democratic_Republic_of_the_Congo_formerly_Zaire("Democratic Republic of the Congo (formerly Zaire)", "COD"),
    Denmark("Denmark", "DNK"),
    Djibouti("Djibouti", "DJI"),
    Dominica("Dominica", "DMA"),
    Dominican_Rep("Dominican Rep.", "DOM"),
    East_Timor("East Timor", "TMP"),
    Ecuador("Ecuador", "ECU"),
    Egypt("Egypt", "EGY"),
    El_Salvador("El Salvador", "SLV"),
    Equatorial_Guinea("Equatorial Guinea", "GNQ"),
    Eritrea("Eritrea", "ERI"),
    Estonia("Estonia", "EST"),
    Ethiopia("Ethiopia", "ETH"),
    Faeroe_Is("Faeroe Is.", "FRO"),
    Falkland_Is_Malvinas("Falkland Is. (Malvinas)", "FLK"),
    Fiji("Fiji", "FJI"),
    Finland("Finland", "FIN"),
    France("France", "FRA"),
    France_Metropolitan("France Metropolitan", "FXX"),
    French_Guiana("French Guiana", "GUF"),
    French_Polynesia("French Polynesia", "PYF"),
    French_Southern_Territory("French Southern Territory", "ATF"),
    Gabon("Gabon", "GAB"),
    Gambia("Gambia", "GMB"),
    Georgia("Georgia", "GEO"),
    Germany("Germany", "DEU"),
    Ghana("Ghana", "GHA"),
    Gibraltar("Gibraltar", "GIB"),
    Greece("Greece", "GRC"),
    Greenland("Greenland", "GRL"),
    Grenada("Grenada", "GRD"),
    Guadeloupe("Guadeloupe", "GLP"),
    Guam("Guam", "GUM"),
    Guatemala("Guatemala", "GTM"),
    Guinea("Guinea", "GIN"),
    Guinea_Bissau("Guinea—Bissau", "GNB"),
    Guyana("Guyana", "GUY"),
    Haiti("Haiti", "HTI"),
    Heard_and_McDonald_Is("Heard and McDonald Is.", "HMD"),
    Holy_See_Vatican_City_State("Holy See (Vatican City State)", "VAT"),
    Honduras("Honduras", "HND"),
    Hong_Kong_China("Hong Kong China", "HKG"),
    Hungary("Hungary", "HUN"),
    Iceland("Iceland", "ISL"),
    India("India", "IND"),
    Indonesia("Indonesia", "IDN"),
    Iran_Islamic_Republic_of("Iran Islamic Republic of", "IRN"),
    Iraq("Iraq", "IRQ"),
    Ireland_Republic_of("Ireland Republic of", "IRL"),
    Israel("Israel", "ISR"),
    Italy("Italy", "ITA"),
    Jamaica("Jamaica", "JAM"),
    Japan("Japan", "JPN"),
    Jordan("Jordan", "JOR"),
    Kazakhstan("Kazakhstan", "KAZ"),
    Kenya("Kenya", "KEN"),
    Kiribati("Kiribati", "KIR"),
    Korea_Democratic_Peoples_Republic_of_North_Korea("Korea Democratic People’s Republic of (North Korea)", "PRK"),
    Korea_Republic_of("Korea Republic of", "KOR"),
    Kuwait("Kuwait", "KWT"),
    Kyrgyzstan("Kyrgyzstan", "KGZ"),
    Lao_Peoples_Democratic_Republic("Lao People’s Democratic Republic", "LAO"),
    Latvia("Latvia", "LVA"),
    Lebanon("Lebanon", "LBN"),
    Lesotho("Lesotho", "LSO"),
    Liberia("Liberia", "LBR"),
    Libyan_Arab_Jamahiriya("Libyan Arab Jamahiriya", "LBY"),
    Liechtenstein("Liechtenstein", "LIE"),
    Lithuania("Lithuania", "LTU"),
    Luxembourg("Luxembourg", "LUX"),
    Macau_China("Macau China", "MAC"),
    Macedonia_the_Former_Yugoslav_Republic_of("Macedonia the Former Yugoslav Republic of", "MKD"),
    Madagascar("Madagascar", "MDG"),
    Malawi("Malawi", "MWI"),
    Malaysia("Malaysia", "MYS"),
    Maldives("Maldives", "MDV"),
    Mali("Mali", "MLI"),
    Malta("Malta", "MLT"),
    Marshall_Islands("Marshall Islands", "MHL"),
    Martinique("Martinique", "MTQ"),
    Mauritania("Mauritania", "MRT"),
    Mauritius("Mauritius", "MUS"),
    Mayotte("Mayotte", "MYT"),
    Mexico("Mexico", "MEX"),
    Micronesia("Micronesia", "FSM"),
    Moldova_Republic_of("Moldova Republic of", "MDA"),
    Monaco("Monaco", "MCO"),
    Mongolia("Mongolia", "MNG"),
    Montserrat("Montserrat", "MSR"),
    Morocco("Morocco", "MAR"),
    Mozambique("Mozambique", "MOZ"),
    Myanmar("Myanmar", "MMR"),
    Namibia("Namibia", "NAM"),
    Nauru("Nauru", "NRU"),
    Nepal("Nepal", "NPL"),
    Netherlands("Netherlands", "NLD"),
    Netherlands_Antilles("Netherlands Antilles", "ANT"),
    New_Caledonia("New Caledonia", "NCL"),
    New_Zealand("New Zealand", "NZL"),
    Nicaragua("Nicaragua", "NIC"),
    Niger("Niger", "NER"),
    Nigeria("Nigeria", "NGA"),
    Niue("Niue", "NIU"),
    Norfolk_Is("Norfolk Is.", "NFK"),
    Northern_Mariana_Islands("Northern Mariana Islands", "MNP"),
    Norway("Norway", "NOR"),
    Oman("Oman", "OMN"),
    Pakistan("Pakistan", "PAK"),
    Palau("Palau", "PLW"),
    Panama("Panama", "PAN"),
    Papua_New_Guinea("Papua New Guinea", "PNG"),
    Paraguay("Paraguay", "PRY"),
    Peru("Peru", "PER"),
    Philippines("Philippines", "PHL"),
    Pitcairn("Pitcairn", "PCN"),
    Poland("Poland", "POL"),
    Portugal("Portugal", "PRT"),
    Puerto_Rico("Puerto Rico", "PRI"),
    Qatar("Qatar", "QAT"),
    Reunion("Reunion", "REU"),
    Romania("Romania", "ROM"),
    Russian_Federation("Russian Federation", "RUS"),
    Russian_Ruble_Domestic("Russian Ruble (Domestic)", "RUS"),
    Rwanda("Rwanda", "RWA"),
    Samoa("Samoa", "WSM"),
    San_Marino("San Marino", "SMR"),
    Sao_Tome_and_Principe("Sao Tome and Principe", "STP"),
    Saudi_Arabia("Saudi Arabia", "SAU"),
    Senegal("Senegal", "SEN"),
    Seychelles("Seychelles", "SYC"),
    Sierra_Leone("Sierra Leone", "SLE"),
    Singapore("Singapore", "SGP"),
    Slovakia("Slovakia", "SVK"),
    Slovenia("Slovenia", "SVN"),

    So_Georgia_and_So_Sandwich_Is("So. Georgia and So. Sandwich Is.", "SGS"),
    Solomon_Is("Solomon Is.", "SLB"),
    Somalia("Somalia", "SOM"),
    South_Africa("South Africa", "ZAF"),
    Spain("Spain", "ESP"),
    Sri_Lanka("Sri Lanka", "LKA"),
    St_Helena("St. Helena", "SHN"),
    St_Kitts_Nevis("St. Kitts-Nevis", "KNA"),
    St_Lucia("St. Lucia", "LCA"),
    St_Pierre_and_Miquelon("St. Pierre and Miquelon", "SPM"),
    St_Vincent_and_The_Grenadines("St. Vincent and The Grenadines", "VCT"),
    Sudan("Sudan", "SDN"),
    Suriname("Suriname", "SUR"),
    Svalbard_and_Jan_Mayen_Is("Svalbard and Jan Mayen Is.", "SJM"),
    Swaziland("Swaziland", "SWZ"),
    Sweden("Sweden", "SWE"),
    Switzerland("Switzerland", "CHE"),
    Syrian_Arab_Rep("Syrian Arab Rep.", "SYR"),
    Taiwan("Taiwan", "TWN"),
    Tajikistan("Tajikistan", "TJK"),
    Tanzania_United_Republic_of("Tanzania United Republic of", "TZA"),
    Thailand("Thailand", "THA"),
    Togo("Togo", "TGO"),
    Tokelau("Tokelau", "TKL"),
    Tonga("Tonga", "TON"),
    Trinidad_and_Tobago("Trinidad and Tobago", "TTO"),
    Tunisia("Tunisia", "TUN"),
    Turkey("Turkey", "TUR"),
    Turkmenistan("Turkmenistan", "TKM"),
    Turks_and_Caicos_Is("Turks and Caicos Is.", "TCA"),
    Tuvalu("Tuvalu", "TUV"),
    US_Minor_Outlying_Islands("U.S. Minor Outlying Islands", "UMI"),
    US_Virgin_Is("U.S. Virgin Is.", "VIR"),
    Uganda("Uganda", "UGA"),
    Ukraine("Ukraine", "UKR"),
    United_Arab_Emirates("United Arab Emirates", "ARE"),
    United_Kingdom("United Kingdom", "GBR"),
    United_States("United States", "USA"),
    Uruguay("Uruguay", "URY"),
    Uzbekistan("Uzbekistan", "UZB"),
    Vanuatu("Vanuatu", "VUT"),
    Venezuela("Venezuela", "VEN"),
    Vietnam("Vietnam", "VNM"),
    Wallis_and_Futuna_Is("Wallis and Futuna Is.", "WLF"),
    Western_Sahara("Western Sahara", "ESH"),
    Yemen("Yemen", "YEM"),
    Yugoslavia("Yugoslavia", "YUG"),
    Zambia("Zambia", "ZMB"),
    Zimbabwe("Zimbabwe", "ZWE");

    private static final Map<String, Country> COUN_TO_ENUM_MAP = new HashMap();
    private static final Map<String, Country> CODE_TO_ENUM_MAP = new HashMap();

    static {
        for (Country type : Country.values()) {
            COUN_TO_ENUM_MAP.put(type.getCountry(), type);
            CODE_TO_ENUM_MAP.put(type.getCountryCode(), type);
        }
    }


    private String country;
    private String countryCode;

    Country(String country, String countryCode) {
        this.country = country;
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public static Country fromCountryCode(String countryCode) {

        if (countryCode == null) {
            return null;
        }

        Country type = CODE_TO_ENUM_MAP.get(countryCode);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for countryCode " + countryCode);
    }

    public static Country fromCountry(String country) {

        if (country == null) {
            return null;
        }

        Country type = COUN_TO_ENUM_MAP.get(country);

        if(type != null) {
            return type;
        }

        throw new IllegalArgumentException("No matching type for country " + country);
    }
}
