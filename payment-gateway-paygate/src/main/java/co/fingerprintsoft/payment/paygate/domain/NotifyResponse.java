package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.*;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Data
@EqualsAndHashCode(of = {"payGateId", "reference", "transactionId"})
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NotifyResponse {

    @NonNull
    private Long payGateId;
    @NonNull
    private String payRequestId;
    @NonNull
    private String reference;
    @NonNull
    private TransactionStatus transactionStatus;
    @NonNull
    private PaymentResult resultCode;
    @NonNull
    private String authCode;
    @NonNull
    private Currency currency;
    @NonNull
    private Long amount;
    @NonNull
    private String resultDesc;
    @NonNull
    private Long transactionId;
    @NonNull
    private String authIndicator;
    @NonNull
    private PaymentMethod payMethod;
    @NonNull
    private String checksum;

    private String payMethodDetail;
    private String user1;
    private String user2;
    private String user3;
    private String vaultId;
    private String payVaultData1;
    private String payVaultData2;
    private Status status;
    private Integer httpStatus;
    private Error error;

    private CardAuthenticationIndicator riskIndicator;

    public NotifyResponse(Long payGateId, String payRequestId, String reference, TransactionStatus transactionStatus, PaymentResult resultCode, String authCode, Currency currency, Long amount, String resultDesc, Long transactionId, String authIndicator, PaymentMethod payMethod, String checksum) {
        this.payGateId = payGateId;
        this.payRequestId = payRequestId;
        this.reference = reference;
        this.transactionStatus = transactionStatus;
        this.resultCode = resultCode;
        this.authCode = authCode;
        this.currency = currency;
        this.amount = amount;
        this.resultDesc = resultDesc;
        this.transactionId = transactionId;
        this.authIndicator = authIndicator;
        this.payMethod = payMethod;
        this.checksum = checksum;
        this.riskIndicator = CardAuthenticationIndicator.fromCode(this.authIndicator);
    }

    public String calculateChecksum(String encryptedKey) throws NoSuchAlgorithmException {

        StringBuilder sb = new StringBuilder();

        sb.append(payGateId);
        sb.append(payRequestId);
        sb.append(reference);
        sb.append(transactionStatus.getTransactionCode());
        sb.append(resultCode.getCode());
        if(authCode != null) {
            sb.append(authCode);
        }
        sb.append(currency.getCurrencyCode());
        sb.append(amount);
        sb.append(resultDesc);
        sb.append(transactionId);
        sb.append(authIndicator);
        if(payMethod != null) {
            sb.append(payMethod.getPayMethod());
        }
        if(StringUtils.isNotBlank(payMethodDetail)) sb.append(payMethodDetail);
        if(StringUtils.isNotBlank(user1)) sb.append(user1);
        if(StringUtils.isNotBlank(user2)) sb.append(user2);
        if(StringUtils.isNotBlank(user3)) sb.append(user3);
        if(StringUtils.isNotBlank(vaultId)) sb.append(vaultId);
        if(StringUtils.isNotBlank(payVaultData1)) sb.append(payVaultData1);
        if(StringUtils.isNotBlank(payVaultData2)) sb.append(payVaultData2);

        sb.append(encryptedKey);
        return MD5ToString.md5String(sb);
    }

    public boolean verifyChecksum(String encryptedKey) throws NoSuchAlgorithmException {
        return this.checksum.equals(calculateChecksum(encryptedKey));
    }

    public void updateChecksum(String encryptedKey) throws NoSuchAlgorithmException {
        this.checksum = calculateChecksum(encryptedKey);
    }

    public static NotifyResponse constructFromMap(Map<String, String> map) {

        NotifyResponse response = new NotifyResponse();
        for (String key : map.keySet()) {
            switch (key) {
                case FieldNameConstants.PAYGATE_ID:
                    response.setPayGateId(Long.valueOf(map.get(key)));
                    break;
                case FieldNameConstants.PAY_REQUEST_ID:
                    response.setPayRequestId(map.get(key));
                    break;
                case FieldNameConstants.REFERENCE:
                    response.setReference(map.get(key));
                    break;
                case FieldNameConstants.TRANSACTION_STATUS:
                    response.setTransactionStatus(TransactionStatus.fromTransactionCode(Integer.parseInt(map.get(key))));
                    break;
                case FieldNameConstants.RESULT_CODE:
                    response.setResultCode(PaymentResult.fromCode(Integer.parseInt(map.get(key))));
                    break;
                case FieldNameConstants.AUTH_CODE:
                    response.setAuthCode(map.get(key));
                    break;
                case FieldNameConstants.CURRENCY:
                    response.setCurrency(Currency.fromCurrencyCode(map.get(key)));
                    break;
                case FieldNameConstants.AMOUNT:
                    response.setAmount(Long.valueOf(map.get(key)));
                    break;
                case FieldNameConstants.RESULT_DESC:
                    response.setResultDesc(map.get(key));
                    break;
                case FieldNameConstants.TRANSACTION_ID:
                    response.setTransactionId(Long.valueOf(map.get(key)));
                    break;
                case FieldNameConstants.RISK_INDICATOR:
                    response.setAuthIndicator(map.get(key));
                    response.setRiskIndicator(CardAuthenticationIndicator.fromCode(map.get(key)));
                    break;
                case FieldNameConstants.PAY_METHOD:
                    response.setPayMethod(PaymentMethod.fromPayMethod(map.get(key)));
                    break;
                case FieldNameConstants.PAY_METHOD_DETAIL:
                    response.setPayMethodDetail(map.get(key));
                    break;
                case FieldNameConstants.USER1:
                    response.setUser1(map.get(key));
                    break;
                case FieldNameConstants.USER2:
                    response.setUser2(map.get(key));
                    break;
                case FieldNameConstants.USER3:
                    response.setUser3(map.get(key));
                    break;
                case FieldNameConstants.VAULT_ID:
                    response.setVaultId(map.get(key));
                    break;
                case FieldNameConstants.PAYVAULT_DATA_1:
                    response.setPayVaultData1(map.get(key));
                    break;
                case FieldNameConstants.PAYVAULT_DATA_2:
                    response.setPayVaultData2(map.get(key));
                    break;
                case FieldNameConstants.CHECKSUM:
                    response.setChecksum(map.get(key));
                    break;
                case FieldNameConstants.ERROR:
                    String errorCode = map.get(key);
                    Error[] errors = Error.values();
                    response.setStatus(Status.FAILURE);
                    for (Error error : errors) {
                        boolean errorBoolean = error.getCode().equals(errorCode);
                        if (errorBoolean) {
                            response.setError(error);
                        }
                    }
                    break;
            }
        }
        if (map.get("ERROR") == null) {
            response.setStatus(Status.SUCCESS);
        }

        return response;
    }


    public String getErrorCode() {
        if (error != null) {
            return error.getCode();
        }
        return null;
    }

    public String getErrorMessage() {
        if (error != null) {
            return error.getMessage();
        }
        return null;
    }


}
