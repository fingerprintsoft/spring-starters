package co.fingerprintsoft.payment.paygate.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PayGateSettings {

    private Long payGateId;
    private String encryptionKey;
    private String returnUrl;
    private String notifyUrl;

}
