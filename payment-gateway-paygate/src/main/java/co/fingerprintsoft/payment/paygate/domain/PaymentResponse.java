package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.FieldNameConstants;
import co.fingerprintsoft.payment.paygate.MD5ToString;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Data
@EqualsAndHashCode(of = {"payGateId", "payRequestId", "reference"})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class PaymentResponse {

    @NonNull
    private Long payGateId;
    @NonNull
    private String payRequestId;
    @NonNull
    private String reference;
    @NonNull
    private String checksum;

    private Status status;

    private Integer httpStatus;

    private Error error;


    public String calculateChecksum(String encryptedKey) throws NoSuchAlgorithmException {

        StringBuilder sb = new StringBuilder();
        sb.append(this.payGateId);
        sb.append(this.payRequestId);
        sb.append(this.reference);
        sb.append(encryptedKey);

        return MD5ToString.md5String(sb);
    }

    public boolean verifyChecksum(String encryptionKey) throws NoSuchAlgorithmException {
        log.debug("Encryption Key: {}", encryptionKey);
        log.debug("Checksum: {}", checksum);
        log.debug("Calculated Checksum: {}", calculateChecksum(encryptionKey));
        if (this.checksum.equals(calculateChecksum(encryptionKey))) {
            return true;
        }
        return false;
    }

    public void updateChecksum(String EncryptionKey) throws NoSuchAlgorithmException {
        this.checksum = calculateChecksum(EncryptionKey);
    }

    public static PaymentResponse constructFromMap(Map<String, String> map) {
        PaymentResponse response = new PaymentResponse();
        for (String key : map.keySet()) {
            switch (key) {
                case FieldNameConstants.PAYGATE_ID:
                    response.setPayGateId(Long.valueOf(map.get(key)));
                    break;
                case FieldNameConstants.PAY_REQUEST_ID:
                    response.setPayRequestId(map.get(key));
                    break;
                case FieldNameConstants.REFERENCE:
                    response.setReference(map.get(key));
                    break;
                case FieldNameConstants.CHECKSUM:
                    response.setChecksum(map.get(key));
                    break;
                case FieldNameConstants.ERROR:
                    String errorCode = map.get(key);
                    Error[] errors = Error.values();
                    response.setStatus(Status.FAILURE);
                    for (Error error : errors) {
                        boolean errorBoolean = error.getCode().equals(errorCode);
                        if (errorBoolean) {
                            response.setError(error);
                        }
                    }
                    break;
            }
            if (map.get("ERROR") == null) {
                response.setStatus(Status.SUCCESS);
            }


        }
        return response;
    }

    public String getErrorCode() {
        if (error != null) {
            return error.getCode();
        }
        return null;
    }

    public String getErrorMessage() {
        if (error != null) {
            return error.getMessage();
        }
        return null;
    }
}
