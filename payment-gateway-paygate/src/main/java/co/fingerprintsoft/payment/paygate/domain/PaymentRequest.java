package co.fingerprintsoft.payment.paygate.domain;

import co.fingerprintsoft.payment.paygate.Country;
import co.fingerprintsoft.payment.paygate.Currency;
import co.fingerprintsoft.payment.paygate.Locale;
import co.fingerprintsoft.payment.paygate.MD5ToString;
import co.fingerprintsoft.payment.paygate.api.PayGateSettings;
import lombok.*;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Data
@EqualsAndHashCode(of = {"payGateId", "reference"})
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaymentRequest {


    @NonNull
    private String reference;
    @NonNull
    private Long amount;
    @NonNull
    private Currency currency;
    @NonNull
    private LocalDateTime transactionDate;
    @NonNull
    private Locale locale;
    @NonNull
    private Country country;
    @NonNull
    private String email;

    private Optional<String> payMethod = Optional.empty();
    private Optional<String> payMethodDetail = Optional.empty();
    private Optional<String> user1 = Optional.empty();
    private Optional<String> user2 = Optional.empty();
    private Optional<String> user3 = Optional.empty();
    private Optional<Integer> vault = Optional.empty();
    private Optional<String> vaultId = Optional.empty();


    public void setPayMethod(String payMethod) {
        this.payMethod = Optional.ofNullable(payMethod);
    }

    public void setPayMethodDetail(String payMethodDetail) {
        this.payMethodDetail = Optional.ofNullable(payMethodDetail);
    }

    public void setUser1(String user1) {
        this.user1 = Optional.ofNullable(user1);
    }

    public void setUser2(String user2) {
        this.user2 = Optional.ofNullable(user2);
    }

    public void setUser3(String user3) {
        this.user3 = Optional.ofNullable(user3);
    }

    public void setVault(Integer vault) {
        this.vault = Optional.ofNullable(vault);
    }

    public void setVaultId(String vaultId) {
        this.vaultId = Optional.ofNullable(vaultId);
    }

    public String calculateChecksum(PayGateSettings settings) throws NoSuchAlgorithmException {


        StringBuilder sb = new StringBuilder();
        sb.append(settings.getPayGateId());
        sb.append(this.reference);
        sb.append(this.amount);
        sb.append(this.currency.getCurrencyCode());
        sb.append(settings.getReturnUrl());
        sb.append(this.getFormattedDate());
        sb.append(this.locale.getCode());
        sb.append(this.country.getCountryCode());
        sb.append(this.email);
        payMethod.ifPresent(sb::append);
        payMethodDetail.ifPresent(sb::append);
        if (settings.getNotifyUrl() != null) {
            sb.append(settings.getNotifyUrl());
        }
        user1.ifPresent(sb::append);
        user2.ifPresent(sb::append);
        user3.ifPresent(sb::append);
        vault.ifPresent(sb::append);
        vaultId.ifPresent(sb::append);
        sb.append(settings.getEncryptionKey());

        return MD5ToString.md5String(sb);
    }

    public String getFormattedDate() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(getTransactionDate());
    }
}






