package co.fingerprintsoft.payment.paygate;

import org.apache.commons.codec.digest.DigestUtils;

import java.security.NoSuchAlgorithmException;

public class MD5ToString {

    public static String md5String(StringBuilder sb) throws NoSuchAlgorithmException {
        return DigestUtils.md5Hex(sb.toString());
    }

}
