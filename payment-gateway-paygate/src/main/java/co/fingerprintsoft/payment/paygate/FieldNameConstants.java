package co.fingerprintsoft.payment.paygate;

public class FieldNameConstants {
    public static final String PAYGATE_ID = "PAYGATE_ID";
    public static final String PAY_REQUEST_ID = "PAY_REQUEST_ID";
    public static final String REFERENCE = "REFERENCE";
    public static final String CHECKSUM = "CHECKSUM";
    public static final String ERROR = "ERROR";
    public static final String AMOUNT = "AMOUNT";
    public static final String CURRENCY = "CURRENCY";
    public static final String RETURN_URL = "RETURN_URL";
    public static final String TRANSACTION_DATE = "TRANSACTION_DATE";
    public static final String LOCALE = "LOCALE";
    public static final String COUNTRY = "COUNTRY";
    public static final String EMAIL = "EMAIL";
    public static final String PAY_METHOD = "PAY_METHOD";
    public static final String PAY_METHOD_DETAIL = "PAY_METHOD_DETAIL";
    public static final String USER1 = "USER1";
    public static final String USER2 = "USER2";
    public static final String USER3 = "USER3";
    public static final String VAULT = "VAULT";
    public static final String VAULT_ID = "VAULT_ID";
    public static final String TRANSACTION_STATUS = "TRANSACTION_STATUS";
    public static final String RESULT_CODE ="RESULT_CODE";
    public static final String AUTH_CODE = "AUTH_CODE";
    public static final String RESULT_DESC ="RESULT_DESC";
    public static final String TRANSACTION_ID ="TRANSACTION_ID";
    public static final String RISK_INDICATOR="RISK_INDICATOR";
    public static final String PAYVAULT_DATA_1 ="PAYVAULT_DATA_1";
    public static final String PAYVAULT_DATA_2 ="PAYVAULT_DATA_2";

}
