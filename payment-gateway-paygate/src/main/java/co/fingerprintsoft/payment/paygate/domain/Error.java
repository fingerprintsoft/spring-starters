package co.fingerprintsoft.payment.paygate.domain;

public enum Error {

    CNTRY_INVALID("CNTRY_INVALID","Invalid Country entered"),
    DATA_AMT_NUM("DATA_AMT_NUM","Invalid Amount entered"),
    DATA_CHK("DATA_CHK","Checksum posted does not match the one calculated by PayGate, either due to an incorrect encryption key used or a field that has been excluded from the checksum calculation"),
    DATA_PW("DATA_PW","Mandatory fields have been excluded from the post to PayGate"),
    INV_EMAIL("INV_EMAIL","Invalid email entered"),
    LOCALE_INVALID("LOCALE_INVALID","Invalid Locale entered"),
    ND_INV_PGID("ND_INV_PGID","Invalid PayGate Id"),
    PGID_NOT_EN("PGID_NOT_EN","The PayGate ID being used to post data to PayGate has not yet been enabled, or there are no payment methods setup on it."),
    DATA_PM("DATA_PM","Invalid Payment Method"),
    INVALID_VAULT("INVALID_VAULT", "Invalid Vault"),
    PGID_MOD("PGID_MOD","The PayGate ID being used to post data to PayGate has been modified during response."),
    ND_INV_PRID("ND_INV_PRID","The PayRequest ID being used to post data to PayGate has not yet been enabled, or there are no payment methods setup on it."),
    DATA_INS("DATA_INS", "Unspecified internet connectivity or web browser issue processing request."),
    DATA_PAY_REQ_ID("DATA_PAY_REQ_ID","The PayRequest ID being used is more than 36 character max size."),
    NO_TRANS_DATA("NO_TRANS_DATA","No transaction data.");

    Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
