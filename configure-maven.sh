#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>fingerprints.maven.snapshots</id>\
  <username>${MAVEN_USERNAME}</username>\
  <password>${MAVEN_PASSWORD}</password>\
</server>\
<server>\
  <id>fingerprints.maven</id>\
  <username>${MAVEN_USERNAME}</username>\
  <password>${MAVEN_PASSWORD}</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>fingerprints.maven</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>fingerprints.maven</id>\
      <url>https://maven.fingerprintsoft.co/repository/maven-public/</url>\
    </repository>\
    <repository>\
      <id>fingerprints.maven.snapshots</id>\
      <url>https://maven.fingerprintsoft.co/repository/maven-public/</url>\
    </repository>\
  </repositories>\
</profile>" /usr/share/maven/conf/settings.xml
