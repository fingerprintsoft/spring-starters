package co.fingerprintsoft.notification;

public interface NotificationResult {

    String getResult();

}
