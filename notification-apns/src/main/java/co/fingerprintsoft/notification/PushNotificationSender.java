package co.fingerprintsoft.notification;



public interface PushNotificationSender {

    /**
     * Sends out the push notification to the given device token
     *
     * @param text push notification text
     * @param deviceToken token of device to send the notification to.
     */
    NotificationResult sendNotification(String text, String deviceToken);
}
