package co.fingerprintsoft.notification.apns;

import lombok.Data;

@Data
public class APNSSettings {

    private String certificatePath;
    private String password;
    private boolean sandboxed = true;

}
