package co.fingerprintsoft.notification.email;


/**
 * Created with IntelliJ IDEA.
 * User: naeem
 * Date: 2014/03/01
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public interface EmailSender {

    EmailResponse sendPlainText(Email email);
    EmailResponse sendComplexMessage(Email email);
}
