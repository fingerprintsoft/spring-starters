package co.fingerprintsoft.notification.email;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class EmailResponse {

    private String id;
    private String message;
    private Integer status;

}
