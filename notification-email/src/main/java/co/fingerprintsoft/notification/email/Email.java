package co.fingerprintsoft.notification.email;

import lombok.Data;
import lombok.ToString;

import java.io.File;
import java.util.List;

@Data
@ToString
public class Email  {

    private String from;
    private String to;
    private String subject;
    private String message;
    private String html;
    private List<File> attachments;
    private String cc;
    private String bcc;


}
