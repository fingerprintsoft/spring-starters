package co.fingerprintsoft.notification;

import co.fingerprintsoft.notification.apns.APNSSenderAutoConfiguration;
import co.fingerprintsoft.notification.bulksms.BulkSMSSMSSenderAutoConfiguration;
import co.fingerprintsoft.notification.clickatell.ClickatellSMSSenderAutoConfiguration;
import co.fingerprintsoft.notification.json.JsonAutoConfiguration;
import co.fingerprintsoft.notification.mailgun.MailgunEmailSenderAutoConfiguration;
import co.fingerprintsoft.notification.pushy.PushyAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(
        {
                JsonAutoConfiguration.class,
                PushyAutoConfiguration.class,
                MailgunEmailSenderAutoConfiguration.class,
                BulkSMSSMSSenderAutoConfiguration.class,
                ClickatellSMSSenderAutoConfiguration.class,
                APNSSenderAutoConfiguration.class,
        }
)
public @interface EnableNotifications {
}
