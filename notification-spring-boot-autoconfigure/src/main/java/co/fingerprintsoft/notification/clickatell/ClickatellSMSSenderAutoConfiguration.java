package co.fingerprintsoft.notification.clickatell;

import com.fasterxml.jackson.databind.ObjectMapper;
import con.fingerprintsoft.notification.sms.SMSSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(ClickatellSMSSender.class)
@EnableConfigurationProperties(ClickatellSMSNotificationProperties.class)
public class ClickatellSMSSenderAutoConfiguration {

    @Autowired
    private ClickatellSMSNotificationProperties notificationProperties;
    @Autowired
    private ObjectMapper objectMapper;

    public ClickatellSMSSenderAutoConfiguration() {
    }

    public ClickatellSMSSenderAutoConfiguration(ClickatellSMSNotificationProperties notificationProperties, ObjectMapper objectMapper) {
        this.notificationProperties = notificationProperties;
        this.objectMapper = objectMapper;
    }

    @Bean
    @ConditionalOnClass(ClickatellSettings.class)
    public ClickatellSettings clickatellSettings() {

        String apiId = notificationProperties.getApiId();
        String url = notificationProperties.getUrl();
        String grantType = notificationProperties.getGrantType();
        String token = notificationProperties.getToken();
        String version = notificationProperties.getVersion();

        ClickatellSettings clickatellSettings = new ClickatellSettings();
        clickatellSettings.setApiId(apiId);
        clickatellSettings.setUrl(url);
        clickatellSettings.setGrantType(grantType);
        clickatellSettings.setToken(token);
        clickatellSettings.setVersion(version);

        return clickatellSettings;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(ClickatellSMSSender.class)
    @ConditionalOnBean({ObjectMapper.class, ClickatellSettings.class})
    public SMSSender smsSender(ClickatellSettings clickatellSettings) {
        return new ClickatellSMSSender(clickatellSettings, objectMapper);
    }



}
