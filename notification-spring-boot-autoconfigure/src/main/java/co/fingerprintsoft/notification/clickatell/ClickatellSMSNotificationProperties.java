package co.fingerprintsoft.notification.clickatell;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix ="notifications.clickatell")
public class ClickatellSMSNotificationProperties {

    private String apiId;
    private String token;
    private String url;
    private String version;
    private String grantType;

}
