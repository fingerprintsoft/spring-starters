package co.fingerprintsoft.notification.apns;

import co.fingerprintsoft.notification.PushNotificationSender;
import com.relayrides.pushy.apns.ApnsClient;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

@Configuration
@ConditionalOnClass(APNSSender.class)
@EnableConfigurationProperties(APNSNotificationProperties.class)
public class APNSSenderAutoConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(APNSSenderAutoConfiguration.class);

    @Autowired
    private APNSNotificationProperties notificationProperties;
    @Autowired
    private ApnsClient apnsClient;

    public APNSSenderAutoConfiguration() {
    }

    public APNSSenderAutoConfiguration(APNSNotificationProperties notificationProperties, ApnsClient apnsClient) {
        this.notificationProperties = notificationProperties;
        this.apnsClient = apnsClient;
    }

    @Bean
    @ConditionalOnClass(APNSSettings.class)
    public APNSSettings apnsSettings() {

        String certificatePath = notificationProperties.getCertificatePath();
        String password = notificationProperties.getPassword();
        Boolean sandboxed = notificationProperties.isSandboxed();

        APNSSettings apnsSettings = new APNSSettings();
        apnsSettings.setCertificatePath(certificatePath);
        apnsSettings.setPassword(password);
        apnsSettings.setSandboxed(sandboxed);

        return apnsSettings;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(APNSSender.class)
    @ConditionalOnBean({APNSSettings.class, ApnsClient.class})
    public PushNotificationSender pushNotificationSender(APNSSettings apnsSettings, ApnsClient apnsClient) {
        return new APNSSender(apnsSettings, apnsClient);
    }

    @PreDestroy
    public void close() {
        final Future<Void> disconnectFuture = apnsClient.disconnect();
        try {
            disconnectFuture.await(10000);
        } catch (InterruptedException e) {
            LOG.error("Error during shutdown", e);
        }
    }

}
