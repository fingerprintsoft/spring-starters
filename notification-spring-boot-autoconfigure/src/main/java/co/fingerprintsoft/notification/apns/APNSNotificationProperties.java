package co.fingerprintsoft.notification.apns;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix ="notifications.apns")
public class APNSNotificationProperties {

    private String certificatePath;
    private String password;
    private boolean sandboxed = true;

}
