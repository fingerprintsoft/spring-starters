package co.fingerprintsoft.notification.mailgun;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix ="notifications.mailgun")
public class MailgunNotificationProperties {

    private String emailFrom;

    private String apiKey;
    private String domain;
    private String version = "v3";
    private String url;
    private String testModeTo;
    private boolean testMode;

}
