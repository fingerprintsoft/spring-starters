package co.fingerprintsoft.notification.mailgun;

import co.fingerprintsoft.notification.email.EmailSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(MailGunSender.class)
@EnableConfigurationProperties(MailgunNotificationProperties.class)
public class MailgunEmailSenderAutoConfiguration {

    @Autowired
    private MailgunNotificationProperties notificationProperties;

    public MailgunEmailSenderAutoConfiguration() {
    }

    public MailgunEmailSenderAutoConfiguration(MailgunNotificationProperties notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    @Bean
    @ConditionalOnClass(MailGunSettings.class)
    public MailGunSettings mailGunSettings() {

        String apiKey = notificationProperties.getApiKey();
        String domain = notificationProperties.getDomain();
        String version = notificationProperties.getVersion();
        String url = notificationProperties.getUrl();
        String from = notificationProperties.getEmailFrom();
        boolean testMode = notificationProperties.isTestMode();
        String to = notificationProperties.getTestModeTo();

        MailGunSettings mailGunSettings = new MailGunSettings();
        mailGunSettings.setApiKey(apiKey);
        mailGunSettings.setDomain(domain);
        mailGunSettings.setVersion(version);
        mailGunSettings.setUrl(url);
        mailGunSettings.setFrom(from);
        mailGunSettings.setTestMode(testMode);
        mailGunSettings.setTestModeTo(to);

        return mailGunSettings;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(MailGunSender.class)
    @ConditionalOnBean({ObjectMapper.class, MailGunSettings.class})
    public EmailSender emailSender(MailGunSettings mailGunSettings, ObjectMapper objectMapper) {
        return new MailGunSender(mailGunSettings, objectMapper);
    }

}
