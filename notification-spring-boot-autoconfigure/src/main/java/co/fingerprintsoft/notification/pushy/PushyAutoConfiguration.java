package co.fingerprintsoft.notification.pushy;

import co.fingerprintsoft.notification.apns.APNSNotificationProperties;
import co.fingerprintsoft.notification.apns.APNSSenderAutoConfiguration;
import com.relayrides.pushy.apns.ApnsClient;
import com.relayrides.pushy.apns.ApnsClientBuilder;
import io.netty.handler.ssl.OpenSsl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@ConditionalOnClass(ApnsClient.class)
@EnableConfigurationProperties(APNSNotificationProperties.class)
public class PushyAutoConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(APNSSenderAutoConfiguration.class);

    @Autowired
    private APNSNotificationProperties notificationProperties;

    public PushyAutoConfiguration() {
    }

    public PushyAutoConfiguration(APNSNotificationProperties notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(ApnsClient.class)
    public ApnsClient apnsClient() {
        try {
            LOG.debug("OpenSSL available? {}", OpenSsl.isAvailable());
            LOG.debug("ALPN available?    {}", OpenSsl.isAlpnSupported());

            if (OpenSsl.unavailabilityCause() != null) {
                LOG.error("Unavailability cause:", OpenSsl.unavailabilityCause().getCause());
                throw new RuntimeException(OpenSsl.unavailabilityCause().getCause());
            }
            Path path = Paths.get(notificationProperties.getCertificatePath());

            return new ApnsClientBuilder()
                    .setClientCredentials(path.toFile(), notificationProperties.getPassword())
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Cannot get the certificate path provided at : " + notificationProperties.getCertificatePath());
        }
    }
}
