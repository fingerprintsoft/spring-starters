package co.fingerprintsoft.notification.bulksms;

import con.fingerprintsoft.notification.sms.SMSSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(BulkSMSSender.class)
@EnableConfigurationProperties(BulkSMSNotificationProperties.class)
public class BulkSMSSMSSenderAutoConfiguration {

    @Autowired
    private BulkSMSNotificationProperties notificationProperties;

    public BulkSMSSMSSenderAutoConfiguration() {
    }

    public BulkSMSSMSSenderAutoConfiguration(BulkSMSNotificationProperties notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    @Bean
    @ConditionalOnClass(BulkSMSSettings.class)
    public BulkSMSSettings bulkSMSSettings() {

        String eapiUrl = notificationProperties.getEapiUrl();
        String username = notificationProperties.getUsername();
        String password = notificationProperties.getPassword();
        Integer testAlwaysFail = notificationProperties.getTestAlwaysFail();
        Integer testAlwaysSucceed = notificationProperties.getTestAlwaysSucceed();

        BulkSMSSettings bulkSMSSettings = new BulkSMSSettings();
        bulkSMSSettings.setEapiUrl(eapiUrl);
        bulkSMSSettings.setUsername(username);
        bulkSMSSettings.setPassword(password);
        bulkSMSSettings.setTestAlwaysFail(testAlwaysFail);
        bulkSMSSettings.setTestAlwaysSucceed(testAlwaysSucceed);

        return bulkSMSSettings;
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnClass(BulkSMSSender.class)
    @ConditionalOnBean({BulkSMSSettings.class})
    public SMSSender smsSender(BulkSMSSettings bulkSMSSettings) {
        return new BulkSMSSender(bulkSMSSettings);
    }

}
