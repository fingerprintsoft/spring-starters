package co.fingerprintsoft.notification.bulksms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix ="notifications.bulksms")
public class BulkSMSNotificationProperties {

    private String eapiUrl = "http://bulksms.2way.co.za/eapi/submission/send_sms/2/2.0";
    private String username;
    private String password;
    private Integer testAlwaysFail;
    private Integer testAlwaysSucceed;


}
